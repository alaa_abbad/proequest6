// ProEquest theme
$(document).ready(function(){
  // For search on professional page
	$('.proequest-search select').selectmenu({
		style:'dropdown',
		menuWidth: 200,
		maxHeight: 250
	});

  // For searches on homepage
	$('.homepage-search-discipline select').selectmenu({
		style:'dropdown',
    width: 266,
		menuWidth: 180,
		maxHeight: 250
	});

	$('.homepage-search-location select').selectmenu({
		style:'dropdown',
    width: 266,
		menuWidth: 240,
		maxHeight: 250
	});

//  Stop floating after full name fields
  $('.cck_fullname_last_wrapper').after('<div class="clearfix" style="clear:both;width:100%;"></div>');

  //  Hide Profession options from amateurs
  // if ($('#edit-field-profile-profession-value option:selected').val() == '0') {
  //   $('#edit-field-profile-profession-value-wrapper').hide();
  // }

	$('.sidebar-last').find('h2.block-title:first').css('margin-top', 20);
	/*
	$('.proequest-search .views-exposed-form').each(function(){
			$(this).find('.views-exposed-widget:last').css({
					float: 'right',
					paddingRight: 0
			});
	});
	*/
});



