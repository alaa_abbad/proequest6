/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function() {
  $('#homepage-horse-button').click(function() {
    changeFormDest();
    });
});

function changeFormDest() {
  // Check value of discipline and send user to proper page
  $discipline = $('#edit-discipline').val();

  switch($discipline)
  {
  case "Hunter":
    $('#homepage-horsesearch').attr('action', '/search/horses/hunters');
    break;

  case "Jumper":
    $('#homepage-horsesearch').attr('action', '/search/horses/jumpers');
    break;

  case "Equitation":
    $('#homepage-horsesearch').attr('action', '/search/horses/equitation');
    break;

  case "Ponies":
    $('#homepage-horsesearch').attr('action', '/search/horses/ponies');
    break;

  default:
    // Eventing and dressage stays on same page
  }

}