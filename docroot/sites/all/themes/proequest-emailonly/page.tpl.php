<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <style type="text/css">
         a:link, a:visited {
         color:#f59a2b;
         font-family: Helvetica, Arial, sans-serif;
         text-decoration:none;
         }
         a:hover {
         color:#f59a2b;
         font-family: Helvetica, Arial, sans-serif;
         }
         td.bullet a:link, td.news-items a:visited {
         color:#767572 !important;
         }
         td.social a {
         color:#f59a2b;
         }
         td.social img {
         padding:12px;
         }
         p {
         color: #767572;
         font-family: Helvetica, Arial, sans-serif;
         line-height: 130%;
         }
         h1 {
         color: #767572;
         font-family: Helvetica, Arial, sans-serif;
         font-size: 20px;
         line-height: 130%;
         }
         h2 {
         color: #767572;
         font-family: Helvetica, Arial, sans-serif;
         font-size: 14px;
         }
         .copyright {
         font-size: 12px;
         padding-top: 6px;
         padding-bottom: 20px;
         margin-top: 0px;
         }
      </style>
   </head>
   <body style="background-color:#efeeea">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
         <tr>
            <td valign="top" bgcolor="#efeeea">
               <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="font: 14px Helvetica, Arial, sans-serif; color: #767572; line-height: 100%;">
                  <tr>
                     <td colspan="2">
                        <table width="600" border="0" cellpadding="0" cellspacing="0">
                           <tr>
                              <td><a href="http://www.proequest.com"><img src="http://www.proequest.com/sites/all/themes/proequest-emailonly/images/ProEquestLogoForEmail.jpg" alt="ProEquest | Professional Equestrian Marketplace" width="600" height="89"  border="0"/></a></td>
                           </tr>
                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td style="padding: 10px 0;"><?php print $content ?></td>
                  </tr>
               </table>
               <table width="600" align="center" style="border-top:1px solid #333; padding:0px 0px;">
                  <tr valign="top">
                     <td align="left" width="50%">
                        <h2 style="color: #767572;font-family: Helvetica, Arial, sans-serif;font-size: 14px;">Get the most from your membership:</h2>
                        <table style="padding-top: 0px">
                           <tr style="margin-left:10px;">
                              <td align="left" valign="middle" width="25">
                              <img src="http://www.proequest.com/sites/all/themes/proequest-emailonly/images/EmailBullet.jpg" border="0">
                              </td>
                              <td class="bullet" width="210" align="left" valign="top" style="font-family: Helvetica, Arial, sans-serif; color: #767572 !important; padding-bottom: 7px; padding-top:7px; padding-left:5px; line-height: 120%;">Complete your Professional Profile</td>
                           </tr>
                           <tr style="margin-left:10px;">
                              <td align="left" valign="middle">
                                <img src="http://www.proequest.com/sites/all/themes/proequest-emailonly/images/EmailBullet.jpg" border="0">
                              </td>
                              <td class="bullet" align="left" valign="top" style="font-family: Helvetica, Arial, sans-serif; color: #767572 !important; padding-bottom: 7px; padding-top:7px; padding-left:5px; line-height: 120%;">Keep your horse listings up to date</td>
                           </tr>
                           <tr style="margin-left:10px;">
                              <td align="left" valign="middle">
                                <img src="http://www.proequest.com/sites/all/themes/proequest-emailonly/images/EmailBullet.jpg" border="0">
                              <td class="bullet" align="left" valign="top" style="font-family: Helvetica, Arial, sans-serif; color: #767572 !important; padding-bottom: 7px; padding-top:7px; padding-left:5px; line-height: 120%;">View news and read the latest events in our
                                 community news area
                              </td>
                           </tr>
                        </table>
                     </td>
                     <td align="left" width="50%">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                           <tr>
                              <td>
                                 <h2 style="color: #767572;font-family: Helvetica, Arial, sans-serif;font-size: 14px;">Connect and share:</h2>
                              </td>
                           </tr>
                           <tr>
                              <td valign="top">
                                 <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                       <td colspan="2" style="height: 25px; padding-top:8px;">
                                          <table>
                                             <tr>
                                                <td><a href="http://facebook.com/proequest"><img src="http://www.proequest.com/sites/all/themes/proequest-emailonly/images/facebook.png" width="32" height="32" alt="Facebook" border="0"/></a> &nbsp;<a href="http://twitter.com/proequest"><img src="http://www.proequest.com/sites/all/themes/proequest-emailonly/images/twitter.png" width="32" height="32" alt="Twitter"  border="0"/></a></td>
                                                <td><span style="display:block; margin-left:10px;" class="article"><a href="http://facebook.com/ProEquest">Facebook.com/ProEquest</a><br />
                                                   <a href="http://twitter.com/proequest">Twitter.com/ProEquest</a></span>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                              </td>
                           </tr>
                        </table>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td width="50%" align="center" bgcolor="#efeeea"><a href="http://www.proequest.com"><img src="http://www.proequest.com/sites/all/themes/proequest/images/ProEquestLogoSmall.png"  border="0"/></a></td>
         </tr>
         <tr>
            <td bgcolor="#efeeea">
               <p class="copyright" align="center">&copy;2012. All rights reserved. | <a href="http://www.proequest.com/advertising">Advertising</a> | <a href="http://www.proequest.com/terms">Terms of Use</a> | <a href="http://www.proequest.com/privacy">Privacy Policy</a></p>
            </td>
         </tr>
      </table>
   </body>
</html>