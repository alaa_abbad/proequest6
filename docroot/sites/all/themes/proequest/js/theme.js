// ProEquest theme
$(document).ready(function(){

  var clone = $('#pid-user-me-profile-profile #user-me-profile-profile-form #edit-submit').clone().wrap('<p>').parent().html();
  $('#pid-user-me-profile-profile #user-me-profile-profile-form').prepend('<div class="theme-button secondary-submit">'+clone+'</div>');

  if($('.cancel-membership').length > 0) {

    $('.billing-info-container').append($('.cancel-membership').css('display', 'inline-block'));
  }

  // For search on professional page
  $('.proequest-search select').selectmenu({
    style:'dropdown',
    menuWidth: 200,
    maxHeight: 250
  });

  // For searches on homepage
  $('.homepage-search-discipline select').selectmenu({
    style:'dropdown',
    width: 266,
    menuWidth: 180,
    maxHeight: 250
  });

  $('.homepage-search-location select').selectmenu({
    style:'dropdown',
    width: 266,
    menuWidth: 240,
    maxHeight: 250
  });

//  Stop floating after full name fields
  $('.cck_fullname_last_wrapper').after('<div class="clearfix" style="clear:both;width:100%;"></div>');

  //  Hide Profession options from amateurs
  // if ($('#edit-field-profile-profession-value option:selected').val() == '0') {
  //   $('#edit-field-profile-profession-value-wrapper').hide();
  // }

  $('.sidebar-last').find('h2.block-title:first').css('margin-top', 20);
  /*
  $('.proequest-search .views-exposed-form').each(function(){
      $(this).find('.views-exposed-widget:last').css({
          float: 'right',
          paddingRight: 0
      });
  });
  */

  // C3 - Edit
  $('.node-type-horse .field-field-horse-image .field-items .field-item').each(function() {
    $(this).find('a').append('<span></span>');
  });

  $('.node-type-job-posting .field-field-job-photo .field-items .field-item').each(function() {
    $(this).find('a').append('<span></span>');
  });

  // enable selectmenu for filters dropdown
  $('.form-filters-wrapper .form-item-dropdown select').selectmenu({
    style:'dropdown',
    width: 160,
    menuWidth: 180,
    maxHeight: 250
  });
  $('.form-filters-wrapper .form-checkboxes-type select').selectmenu({
    style:'dropdown',
    width: 110,
    menuWidth: 110,
    maxHeight: 250
  });
});



