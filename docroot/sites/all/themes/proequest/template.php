<?php
// $Id$

  if (!isset($_SERVER["HTTPS"])) {
    drupal_add_js("$(document).ready(function(){var a=$('a#p6');a.css({position:'relative',display:'inline-block',background:'transparent'}).append('<img src=\'//p6d.com/assets/20100521.png\' width=\'191\' height=\'49\'>');var i=$('a#p6 img');i.css({border:'0 none',left:'1em',position:'absolute',top:'-45px',zIndex:'999999',display:'none',background:'transparent'});a.hover(function(){i.animate({left:'5px',opacity:'show'},{duration:500,queue:false});},function(){i.animate({left:'0px',opacity:'hide'},{duration:500,queue:false});});});", 'inline');
  }


/**
* Registers overrides for various functions.
* In this case, overrides three user functions
*/
function proequest_theme() {
  return array(
    'user_login' => array(
      'template' => 'user-login',
      'arguments' => array('form' => NULL),
    ),
    'user_register' => array(
      'template' => 'user-register',
      'arguments' => array('form' => NULL),
    ),
    'user_pass' => array(
      'template' => 'user-pass',
      'arguments' => array('form' => NULL),
    ),
    'views_exposed_form__horsesearch__page_1' => array(
      'template' => 'views-exposed-form--horsesearch--page-1',
      'arguments' => array('form' => NULL),
    ),
    'views_exposed_form__search_professional__page_1' => array(
      'template' => 'views-exposed-form--search-professional--page-1',
      'arguments' => array('form' => NULL),
    ),
  );
}
function proequest_preprocess_views_exposed_form__horsesearch__page_1(&$variables) {
  $variables['widgets']['filter-field_horse_location2_lid']->widget = str_replace('>All<', '>- Any Location -<', $variables['widgets']['filter-field_horse_location2_lid']->widget);
  $variables['reset'] = l(t('Clear'), $_GET['q'], array('attributes' => array('class' => 'btn btn-clear')));

  // add a path variable
  $variables['path'] = strtolower($_GET['q']);
}
function proequest_preprocess_views_exposed_form__search_professional__page_1(&$variables) {
  $variables['widgets']['filter-field_horse_location2_lid']->widget = str_replace('>All<', '>- Any Location -<', $variables['widgets']['filter-field_horse_location2_lid']->widget);
  $variables['reset'] = l(t('Clear'), $_GET['q'], array('attributes' => array('class' => 'btn btn-clear')));

  // add a path variable
  $variables['path'] = strtolower($_GET['q']);
}

function proequest_preprocess_views_view(&$vars) {
  if ($vars['name'] == 'jobs' && $vars['display_id'] == 'block_2') {
    $tabs = array(
      'user/me' => t('Jobs !count', array('!count' => '<span>(' . $vars['view']->total_rows . ')</span>'))
    );

    $tab_block = '<ul class="quicktabs-style-proequest">';
    foreach ($tabs as $path => $title) {
      $tab_block .= '<li>';
      $tab_block .= l($title, $path, array('html' => TRUE));
      $tab_block .= '</li>';
    }
    $tab_block .= '</ul>';

    $tab_block .= l(t('Add Job !icon', array('!icon' => '<i class="icon"></i>')), 'jobs/add', array(
      'html' => TRUE,
      'attributes' => array('class' => 'btn-add-job'),
    ));

    $vars['header'] = $tab_block;
  }

}


function proequest_preprocess_user_login(&$variables) {
  $currentPath = $_SERVER['REQUEST_URI'];
  if ($currentPath === '/jobs/add') {
    $variables['template_files'][0] = 'user-login-job-board';
  }
  $variables['intro_text'] = 'Forgot your password? Click <a href="/user/password">here</a> to reset it.'; //t('This is my login form'); //theme_get_setting('userform_login_intro');
  $variables['rendered'] = drupal_render($variables['form']);
}

function proequest_preprocess_user_register(&$variables) {
  $variables['intro_text'] = ''; //t('This is my reg form'); //theme_get_setting('userform_reg_intro');
  $variables['rendered'] = drupal_render($variables['form']);
}

function proequest_preprocess_user_pass(&$variables) {
  $variables['intro_text'] = ''; //t('This is my password form'); //theme_get_setting('userform_pass_intro');
  $variables['rendered'] = drupal_render($variables['form']);
}

/**
 * Modify theme variables
 */
function proequest_preprocess(&$vars, $hook) {
  global $user;                                            // Get the current user
  $vars['logged_in'] = ($user->uid > 0) ? TRUE : FALSE;
  $vars['is_admin'] = in_array('admin', $user->roles);     // Check for Admin, logged in
  $vars['is_professional'] = in_array('Professional', $user->roles);     // Check for Professional
  $vars['is_amateur'] = in_array('Amateur', $user->roles);     // Check for Amateur
  $vars['is_vendor'] = in_array('Vendor', $user->roles);     // Check for Vendor

  $vars['copyright'] = '<div id="copyright" class="clearfix">' .
                       '&copy;' . date("Y") . ' ' . $site_name . ' ' . t('All rights reserved.') .
                       '<span>' . l(t('Advertising'), 'advertising') . '</span>' .
                       '<span>' . l(t('Terms of Use'), 'node/21') . '</span>' .
                       '<span>' . l(t('Privacy Policy'), 'node/24') . '</span>' .
                       '</div>';

  $vars['project6']  = l('Website Credits', 'http://www.project6.com/', array('attributes' => array('id' => 'p6', 'title' => 'Website design & development by Project6 Design, Inc.'), 'query' => 'source=' . $_SERVER['HTTP_HOST']));



  // AVA Removed 					<a class="addthis_button_tweet"></a>
  // AVA Removed 					<a class="addthis_counter addthis_pill_style"></a>

}
function proequest_preprocess_page(&$vars) {

  jquery_ui_add('ui.core');

  $body_classes = array();
  $body_classes[] = ($vars['is_admin']) ? 'is-admin' : '';
  $body_classes[] = ($vars['is_professional']) ? 'is-professional' : '';
  $body_classes[] = ($vars['is_amateur']) ? 'is-amateur' : '';
  $body_classes[] = ($vars['is_vendor']) ? 'is-vendor' : '';

  $path_all = drupal_get_path_alias($_GET['q']);
  $path_all = strtr($path_all, array(' ' => '-', '_' => '-', '/' => '-', '[' => '-', ']' => ''));
  $path_all = preg_replace('/[^\x{002D}\x{0030}-\x{0039}\x{0041}-\x{005A}\x{005F}\x{0061}-\x{007A}\x{00A1}-\x{FFFF}]/u', '', $path_all);
  $body_classes[] = 'path-' . $path_all;

  $currentPath = $_SERVER['REQUEST_URI'];
  if ($currentPath === '/jobs/add' && $path_all == 'user') {
    $vars['title'] = 'Job Board';
    $body_classes[] = 'job-board-login';
    $vars['template_files'][0] = 'page-user-login-job-board';
  }

  $path_first = explode('/', $_SERVER['REQUEST_URI']);
  //kpr($path_first);
  if(isset($path_first['1'])){
    $body_classes[] = 'pathone-' . $path_first['1'];
    if(is_int($path_first['2']) || $path_first['2'] == 'me' || !isset($path_first['3'])) {
      $body_classes[] = 'path-detail-' . $path_first['1'];
    }
  }



  $body_classes = array_filter($body_classes);

  $vars['body_classes'] .= ' ' . implode(' ', $body_classes);

  $vars['footer_links'] = menu_navigation_links('menu-footer-links');

  // For some reason the Menu Local Task on users doesn't work...
  // And I don't have the time to debug that.
  // So here's a quick fix for three pages

  if(end($vars['template_files']) == 'page-user-edit-ms_core_billing_info' || end($vars['template_files']) == 'page-user-me-edit-ms_core_billing_info') {
    $vars['title'] = 'Billing Info';
  }
  else if (end($vars['template_files']) == 'page-user-order-history' || end($vars['template_files']) == 'page-user-me-order-history') {
    $vars['title'] = 'Order History';
  }
  else if (end($vars['template_files']) == 'page-user-member-info' || end($vars['template_files']) == 'page-user-me-member-info') {
    $vars['title'] = 'Current Membership';
  }
  else if(end($vars['template_files']) == 'page-user-me-edit') {
    $vars['title'] = 'Account Info';
  }

}



/**
 * Process variables for nodeasblock.tpl.php.
 *
 * The $variables array contains the following arguments:
 * - $node
 *
 * @see nodeasblock.tpl.php
 */
function proequest_preprocess_nodeasblock(&$variables) {
  $node = $variables['node'];
  // If the user has permissions to edit the node, then add a link.
  if (node_access('update', $node)) {
    $variables['edit_link'] =  l('['. t('edit') .']', 'node/'. $node->nid .'/edit', array("title" => t("Edit")));
  }
 #$variables['content'] = $variables['node']->body;
  $variables['content'] = $variables['node']->nid;

}



/**
 * Set custom form button
 */
function proequest_button($element) {
  // Make sure not to overwrite classes.
  if (isset($element['#attributes']['class'])) {
    $element['#attributes']['class'] = 'form-' . $element['#button_type'] . ' ' . $element['#attributes']['class'];
  }
  else {
    $element['#attributes']['class'] = 'form-' . $element['#button_type'];
  }

  $theme_button_dark = '';

  switch (safe_name(check_plain($element['#value']))):
    case 'log-in':
    case 'post':
      $theme_button_dark = ' theme-button-dark';
      break;
    default:
      $theme_button_dark = '';
  endswitch;

  return '<span class="theme-button' . $theme_button_dark . '"><button type="submit" '
          .(empty($element['#name']) ? '' : 'name="'. $element['#name'].'" ')
          .'id="'. $element['#id'] .'" '
          .'value="'. check_plain($element['#value']) .'" '
          .drupal_attributes($element['#attributes']) .'>'
          .'<span><span>'
          .check_plain($element['#value'])
          .'</span></span>'
          .'</button></span>';
}


function proequest_init_fbjs() {
  static $init;
  if (!$init) {
    $init = true;
    return
      '<div id="fb-root"></div>'.
      '<script>'.
        '(function(d, s, id) {'.
          'var js, fjs = d.getElementsByTagName(s)[0];'.
          'if (d.getElementById(id)) {return;}'.
          'js = d.createElement(s);'.
          'js.id = id;'.
          'js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";'.
          'fjs.parentNode.insertBefore(js, fjs);'.
        '}(document, \'script\', \'facebook-jssdk\'));'.
      '</script>';
  }
  return '';
}

function proequest_fb_recommend_button($send = false, $href = null) {
  return proequest_fb_button('recommend', $send, $href);
}

function proequest_fb_like_button($send = false, $href = null) {
  return proequest_fb_button('like', $send, $href);
}

function proequest_fb_button($action, $send = true, $href = null) {
  $html = proequest_init_fbjs();
  if ($href) {
    $href = 'data-href="'.htmlspecialchars($href, ENT_COMPAT).'" ';
  }
  $html .=
    '<div '.
      'class="fb-like" '.
      $href.
      'data-send="'.($send ? 'true' : 'false').'" '.
      'data-layout="button_count" '.
      'data-show-faces="false" '.
      'data-action="'.$action.'">'.
     '</div>';

  return $html;
}



define ('PROEQUEST_URL_SHARE_TWEET', 'https://twitter.com/share');
function proequest_tweet_button($data_url = null, $data_title = null) {
  $data_url = 'data-url="'. $data_url . '" ';
  $data_title = 'data-text="'.htmlspecialchars($data_title, ENT_COMPAT).'" ';

  return
    '<a href="' . PROEQUEST_URL_SHARE_TWEET . '" class="twitter-share-button" ' . $data_url . $data_title . 'data-via="ProEquest" data-count="horizontal">Tweet</a>'.
    '<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>';
}


//$proequest_og_metadata = array();
function proequest_get_og_metadata() {
  global $proequest_og_metadata;
  if (!$proequest_og_metadata) {
    $proequest_og_metadata = array();
  }
  $meta_tags = '';
  foreach ($proequest_og_metadata as $property => $content_array) {
    foreach ($content_array as $content) {
      $meta_tags .= '<meta property="'.$property.'" content="'.$content.'" />';
    }
  }
  return $meta_tags;
}

function proequest_set_og_metadata($property, $content) {
  global $proequest_og_metadata;
  if (!$proequest_og_metadata) {
    $proequest_og_metadata = array(
      'og:site_name' => array('ProEquest'),
      'og:url' => array(strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'].$_SERVER['REDIRECT_URL']),
    );
  }
  if (!$proequest_og_metadata[$property]) {
    $proequest_og_metadata[$property] = array();
  }
  $proequest_og_metadata[$property][] = trim(str_replace(array('&nbsp;', "\n"), ' ', strip_tags($content)));
}

function proequest_fb_comments($href, $width, $count = '') {
  proequest_set_og_metadata('fb:app_id', '279646535422557');
  if ($count) {
    $count = 'data-num-posts="'.$count.'" ';
  }
  return
    proequest_init_fbjs().
    '<div '.
      'class="fb-comments" '.
      'data-href="'.$href.'" '.
      $count.
      'data-width="'.$width.'">'.
    '</div>';
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 * - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 * - Replaces any character except A-Z, numbers, and underscores with dashes.
 * - Converts entire string to lowercase.
 *
 * @param $string
 *   The string
 * @return
 *   The converted string
 */
function safe_name($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  //	If the first character is not a-z, add 'n' in front.
  //  if (!ctype_lower($string{0})) { // Don't use ctype_alpha since its locale aware.
  //    $string = 'id' . $string;
  //  }
  return $string;
}




/**
 * Theme preset tabs
 *
 * @param $tabs array of available presets
 * @param fid file id
 * @param $presetid preset to highlight
 * @return $output html of the tabs
 */
function proequest_presettabs($presets, $fid, $presetid, $module = '', $field = '', $node_type = '') {
/*
  $module = $module ? ('/'. $module) : '';
  $field = $field ? ('/'. $field) : '';
  $node_type = $node_type ? ('/'. $node_type) : '';

  $tabs[0] = array(
    'data' => t('Select a preset to crop &raquo;'),
    'class' => 'preset-label'
  );

  foreach ($presets['tabs'] as $key => $value) {

    $key++;
    $class = ($value['id'] == $presetid) ? 'imagecrop_highlight' : '';
    $url = 'imagecrop/showcrop/'. $fid .'/'. $value['id'] . $module . $field . $node_type;
    $tabs[$key] = array(
      'data' => l($value['name'], $url),
    );

    if ($value['id'] == $presetid) {
      $tabs[$key]['class'] = 'active';
    }

  }
  return '<div id="imagecrop_presettabs">'. theme('item_list', $tabs, NULL, 'ul', array('id' => 'preset-tabs')) .'<br style="clear: both;"/></div>';
*/
  return '';
}



/**
 * Format the "Submitted by username on date/time" for each node
 *
 * @ingroup themeable
 */
function proequest_node_submitted($node) {
  //return t('Submitted by !username on @datetime',
  return t('Submitted on @datetime',
    array(
      //'!username' => theme('username', $node),
      '@datetime' => format_date($node->created),
    ));
}

/*
 * Override pager
 */

//function phptemplate_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
//  global $pager_page_array, $pager_total;
//
//  // Calculate various markers within this pager piece:
//  // Middle is used to "center" pages around the current page.
//  $pager_middle = ceil($quantity / 2);
//  // current is the page we are currently paged to
//  $pager_current = $pager_page_array[$element] + 1;
//  // first is the first page listed by this pager piece (re quantity)
//  $pager_first = $pager_current - $pager_middle + 1;
//  // last is the last page listed by this pager piece (re quantity)
//  $pager_last = $pager_current + $quantity - $pager_middle;
//  // max is the maximum page number
//  $pager_max = $pager_total[$element];
//  // End of marker calculations.
//
//  // Prepare for generation loop.
//  $i = $pager_first;
//  if ($pager_last > $pager_max) {
//    // Adjust "center" if at end of query.
//    $i = $i + ($pager_max - $pager_last);
//    $pager_last = $pager_max;
//  }
//  if ($i <= 0) {
//    // Adjust "center" if at start of query.
//    $pager_last = $pager_last + (1 - $i);
//    $i = 1;
//  }
//  // End of generation loop preparation.
//
//  $attributes = array("html" => true);
//  //$li_first = theme('pager_first', (isset($tags[0]) ? $tags[0] : t('« first')), $limit, $element, $parameters);
//  $button_previous = '<img src="/' . path_to_theme() .'/images/button_previous.png"></img>';
//  $button_next = '<img src="/' . path_to_theme() .'/images/button_next.png"></img>';
//
//  xdebug_break();
//
//  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : $button_previous), $limit, $element, 1, $attributes);
//  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : $button_next), $limit, $element, 1, $attributes);
//  //$li_last = theme('pager_last', (isset($tags[4]) ? $tags[4] : t('last »')), $limit, $element, $parameters);
//
//  if ($pager_total[$element] > 1) {
////    if ($li_first) {
////      $items[] = array(
////        'class' => 'pager-first',
////        'data' => $li_first,
////      );
////    }
//
//    // When there is more than one page, create the pager list.
//    if ($i != $pager_max) {
//      if ($i > 1) {
//        $items[] = array(
//          'class' => 'pager-ellipsis',
//          'data' => '…',
//        );
//      }
//      // Now generate the actual pager piece.
//      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
//        if ($i < $pager_current) {
//          $items[] = array(
//            'class' => 'pager-item',
//            'data' => theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters),
//          );
//        }
//        if ($i == $pager_current) {
//          $items[] = array(
//            'class' => 'pager-current',
//            'data' => $i,
//          );
//        }
//        if ($i > $pager_current) {
//          $items[] = array(
//            'class' => 'pager-item',
//            'data' => theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters),
//          );
//        }
//      }
//      if ($i < $pager_max) {
//        $items[] = array(
//          'class' => 'pager-ellipsis',
//          'data' => '…',
//        );
//      }
//    }
////    if ($li_last) {
////      $items[] = array(
////        'class' => 'pager-last',
////        'data' => $li_last,
////      );
////    }
//
//    if ($li_previous) {
//      $pager = $li_previous;
//    }
//
//    $pager .= theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));
//
//    if ($li_next) {
//      $pager .= $li_next;
//    }
//    return $pager;
//  }
//}


function phptemplate_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 9) {
  global $pager_page_array, $pager_total;

  // Calculate various markers within this pager piece:
  // Middle is used to "center" pages around the current page.
  $pager_middle = ceil($quantity / 2);
  // current is the page we are currently paged to
  $pager_current = $pager_page_array[$element] + 1;
  // first is the first page listed by this pager piece (re quantity)
  $pager_first = $pager_current - $pager_middle + 1;
  // last is the last page listed by this pager piece (re quantity)
  $pager_last = $pager_current + $quantity - $pager_middle;
  // max is the maximum page number
  $pager_max = $pager_total[$element];
  // End of marker calculations.

  // Prepare for generation loop.
  $i = $pager_first;
  if ($pager_last > $pager_max) {
    // Adjust "center" if at end of query.
    $i = $i + ($pager_max - $pager_last);
    $pager_last = $pager_max;
  }
  if ($i <= 0) {
    // Adjust "center" if at start of query.
    $pager_last = $pager_last + (1 - $i);
    $i = 1;
  }
  // End of generation loop preparation.

  $button_previous = '&laquo;';
  $button_next = '&raquo;';

  //$li_first = theme('pager_first', (isset($tags[0]) ? $tags[0] : t('« first')), $limit, $element, $parameters);
  $li_previous = theme('pager_previous', (isset($tags[1]) ? $tags[1] : $button_previous), $limit, $element, 1, $parameters);
  $li_next = theme('pager_next', (isset($tags[3]) ? $tags[3] : $button_next), $limit, $element, 1, $parameters);
  //$li_last = theme('pager_last', (isset($tags[4]) ? $tags[4] : t('last »')), $limit, $element, $parameters);

  if ($pager_total[$element] > 1) {
    if ($li_first) {
      $items[] = array(
        'class' => 'pager-first',
        'data' => $li_first,
      );
    }
    if ($li_previous) {
      $items[] = array(
        'class' => 'pager-previous',
        'data' => $li_previous,
      );
    }

    // When there is more than one page, create the pager list.
    if ($i != $pager_max) {
      if ($i > 1) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '…',
        );
      }
      // Now generate the actual pager piece.
      for (; $i <= $pager_last && $i <= $pager_max; $i++) {
        if ($i < $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('pager_previous', $i, $limit, $element, ($pager_current - $i), $parameters),
          );
        }
        if ($i == $pager_current) {
          $items[] = array(
            'class' => 'pager-current',
            'data' => $i,
          );
        }
        if ($i > $pager_current) {
          $items[] = array(
            'class' => 'pager-item',
            'data' => theme('pager_next', $i, $limit, $element, ($i - $pager_current), $parameters),
          );
        }
      }
      if ($i < $pager_max) {
        $items[] = array(
          'class' => 'pager-ellipsis',
          'data' => '…',
        );
      }
    }
    // End generation.
    if ($li_next) {
      $items[] = array(
        'class' => 'pager-next',
        'data' => $li_next,
      );
    }
    if ($li_last) {
      $items[] = array(
        'class' => 'pager-last',
        'data' => $li_last,
      );
    }
    return theme('item_list', $items, NULL, 'ul', array('class' => 'pager'));
  }
}



/**
 * Format a "previous page" link. Override from pager.inc
 *
 * @param $text
 *   The name (or image) of the link.
 * @param $limit
 *   The number of query results to display per page.
 * @param $element
 *   An optional integer to distinguish between multiple pagers on one page.
 * @param $interval
 *   The number of pages to move backward when the link is clicked.
 * @param $parameters
 *   An associative array of query string parameters to append to the pager links.
 * @return
 *   An HTML string that generates this piece of the query pager.
 *
 * @ingroup themeable
 */
function phptemplate_pager_previous($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array;
  $output = '';

  // If we are anywhere but the first page
  if ($pager_page_array[$element] > 0) {
    $page_new = pager_load_array($pager_page_array[$element] - $interval, $element, $pager_page_array);

    // If the previous page is the first page, mark the link as such.
    if ($page_new[$element] == 0) {
      $output = theme('pager_first', $text, $limit, $element, $parameters);
    }
    // The previous page is not the first page.
    else {
      $output = theme('pager_link', $text, $page_new, $element, $parameters);
    }
  }

  return $output;
}

/**
 * Format a "next page" link. Override from pager.inc.
 *
 * @param $text
 *   The name (or image) of the link.
 * @param $limit
 *   The number of query results to display per page.
 * @param $element
 *   An optional integer to distinguish between multiple pagers on one page.
 * @param $interval
 *   The number of pages to move forward when the link is clicked.
 * @param $parameters
 *   An associative array of query string parameters to append to the pager links.
 * @return
 *   An HTML string that generates this piece of the query pager.
 *
 * @ingroup themeable
 */
function phptemplate_pager_next($text, $limit, $element = 0, $interval = 1, $parameters = array()) {
  global $pager_page_array, $pager_total;
  $output = '';

  // If we are anywhere but the last page
  if ($pager_page_array[$element] < ($pager_total[$element] - 1)) {
    $page_new = pager_load_array($pager_page_array[$element] + $interval, $element, $pager_page_array);
    // If the next page is the last page, mark the link as such.
    if ($page_new[$element] == ($pager_total[$element] - 1)) {
      $output = theme('pager_last', $text, $limit, $element, $parameters);
    }
    // The next page is not the last page.
    else {
      $output = theme('pager_link', $text, $page_new, $element, $parameters);
    }
  }

  return $output;
}


/**
 * Format a link to a specific query result page. Override of theme_pager_link.
 *
 * @param $page_new
 *   The first result to display on the linked page.
 * @param $element
 *   An optional integer to distinguish between multiple pagers on one page.
 * @param $parameters
 *   An associative array of query string parameters to append to the pager link.
 * @param $attributes
 *   An associative array of HTML attributes to apply to a pager anchor tag.
 * @return
 *   An HTML string that generates the link.
 *
 * @ingroup themeable
 */
function phptemplate_pager_link($text, $page_new, $element, $parameters = array(), $attributes = array()) {
  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query[] = drupal_query_string_encode($parameters, array());
  }
  $querystring = pager_get_querystring();
  if ($querystring != '') {
    $query[] = $querystring;
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    else if (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

  $test = strpos($text, '<img');
  if ($test == 0) {
    return l($text, $_GET['q'], array('attributes' => $attributes, 'query' => count($query) ? implode('&', $query) : NULL, 'html' => true));
  }
  else {
    return l($text, $_GET['q'], array('attributes' => $attributes, 'query' => count($query) ? implode('&', $query) : NULL));
  }
}

function phptemplate_preprocess_node(&$vars, $hook) {
  $function = __FUNCTION__ . '_' . $vars['node']->type;
  if (function_exists($function)) {
    $function($vars);
  }
}

function phptemplate_preprocess_node_story(&$vars) {
  // not using full name, use node author instead
  //  $node = $vars['node'];
  //  $user = user_load($node->uid);
  //  $profile = content_profile_load('profile', $user->uid);
  //  $fullname = $profile->field_profile_name[0]['first'] . " " . $profile->field_profile_name[0]['last'];

  $story_date = format_date($vars['created'], 'custom', 'F j, Y');

  $terms = 'Posted in ' . $vars['terms'];
  $terms = str_replace('/category/news-type/grand-prix-results', '/news/grand-prix-results', $terms);
  $terms = str_replace('/category/news-type/industry-news', '/news/industry-news', $terms);
  $terms = str_replace('/category/news-type/road-to-london', '/news/road-to-london', $terms);
  $terms = str_replace('/category/news-type/grooms-perspective', '/news/grooms-perspective', $terms);
  $terms = str_replace('/category/news-type/coast-to-coast', '/news/coast-to-coast', $terms);
  $terms = str_replace('/category/news-type/the-horse-business', '/news/the-horse-business', $terms);

  $by = "By " . $vars['field_post_author'][0]['value'];

  $vars['byline'] = $story_date . ' | ' . $terms . ' | ' . $by;

}

function phptemplate_preprocess_node_grandprix(&$vars) {
  // Should look and act identical to story node.
  // GrandPrix actually should not be a unique node but
  // we'll wait for the upgrade to D7 to merge them.
  phptemplate_preprocess_node_story($vars);
}

// C3 Edit
//------------------------------------------------------------------------------

/**
 * Implements template_preprocess_node().
 */
function proequest_preprocess_node(&$variables, $hook) {
  $function = 'proequest_preprocess_node'.'_'. $variables['node']->type;
  if (function_exists($function)) {
   $function($variables);
  }
}


/**
 * Implements template_preprocess_node_TYPE().
 */
function proequest_preprocess_node_horse(&$variables) {
  // add location
  $variables['field_horse_location'][0]['value'] = $variables['field_horse_location2'][0]['city'] . ', ' . $variables['field_horse_location2'][0]['province'] . ' ' . $variables['field_horse_location2'][0]['country_name'];

  // use the full template if is page
  if ($variables['page'] == 1) {
    $node = $variables['node'];
    $poster = user_load($variables['node']->uid);
    $profile = content_profile_load('profile', $poster->uid);

    $variables['content'] = node_build_content($variables['node'], FALSE, TRUE);
    $variables['template_files'][0] = 'node-horse-full';
    $variables['profile'] = $profile;
    $variables['contact_button'] = l(t('Contact the seller'), 'mailto:' . $profile->field_contact_email[0]['value'], array(
      'query' => array(
        'subject' => 'ProEquest Enquiry: ' . $variables['node']->title,
      ),
      'absolute' => TRUE,
      'attributes' => array(
        'class' => 'btn',
      ),
    ));

    // set metadata
    proequest_set_og_metadata('og:title', $node->title);
    proequest_set_og_metadata('og:type', 'athlete');
    proequest_set_og_metadata('og:description', $node->field_horse_desc[0]['value']);
    if ($node->field_horse_image[0]['filepath']) {
      $host = strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'].'/';
      foreach ($node->field_horse_image as $image) {
        if ($image['filepath']) {
          proequest_set_og_metadata('og:image', $host.$image['filepath']);
        }
      }
    }
  }
}

/**
 * Implements template_preprocess_node_TYPE().
 */
function proequest_preprocess_node_profile(&$variables) {
  // use the teaser template if not page
  if ($variables['page'] != 1) {
    $variables['template_files'][0] = 'node-profile-teaser';
  }
}

/**
 * Implements template_preprocess_node_TYPE().
 */
function proequest_preprocess_node_job_posting(&$variables) {
  $submitted = explode(' - ', $variables['date']);
  $timestamp = strtotime($submitted[0]);
  $submitted = date('m.d.y', $timestamp);
  $variables['date'] = $submitted;
  $variables['submitted'] = 'Posted: ' . $submitted;
}

function proequest_render_fields($variables, $fields, $type = '') {

  $output = '';

  foreach ($fields as $field_name) {
    if ($variables['field_horse_' . $field_name][0]['value'] != '') { // assume field is not empty if first value is set
      $output .= '<div class="field field-' . $type . '">';
      if (proequest_field_labels($field_name)) {
        $output .= '<div class="field-label">';
        $output .= t(proequest_field_labels($field_name)) . ':';
        $output .= '</div>';
      }
      $output .= '<div class="field-value">';
      $field_values = array();
      foreach ($variables['field_horse_' . $field_name] as $field_value) {
        $field_values[] = $field_value['value'];
      }
      $output .= implode(', ', $field_values);
      $output .= '</div>';
      $output .= '</div>';
    }
  }

  return $output;
}

function proequest_field_labels($field_name) {
  $labels = array(
    'hunters' => 'Hunter',
    'jumpers' => 'Jumper',
    'equitation' => 'Equitation',
    'eventers' => 'Eventers',
    'ponies' => 'Ponies',
    'dressage' => 'Dressage',
    'age' => 'Born',
    'height' => 'Height',
    'sex' => 'Sex',
    'breed' => 'Breed',
    'location' => 'Location',
    'price' => 'Price',
  );
  return isset($labels[$field_name]) ? $labels[$field_name] : FALSE;
}

function proequest_form_element($element, $value) {
  // This is also used in the installer, pre-database setup.
  $t = get_t();

$output = '<div style="width:400px" class="form-item beautytips" title="'.htmlentities($element['#description']).'"';
  if (!empty($element['#id'])) {
    $output .= ' id="' . $element['#id'] . '-wrapper"';
  }
  $output .= ">\n";
  $required = !empty($element['#required']) ? '<span class="form-required" title="' . $t('This field is required.') . '">*</span>' : '';

  if (!empty($element['#title'])) {
    $title = $element['#title'];
    if (!empty($element['#id'])) {
      $output .= ' <label for="' . $element['#id'] . '">' . $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) . "</label>\n";
    }
    else {
      $output .= ' <label>' . $t('!title: !required', array('!title' => filter_xss_admin($title), '!required' => $required)) . "</label>\n";
    }
  }
  $output .= $value;

/*  if (!empty($element['#description'])) {
    $output .= ' <div class="description">' . $element['#description'] . "</div>\n";
  }
*/
  $output .= "</div>\n";

  return $output;
}
