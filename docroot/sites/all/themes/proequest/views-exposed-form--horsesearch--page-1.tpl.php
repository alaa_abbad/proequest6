<div class="form-filters-wrapper">
  <div class="col-top">
    <h4><?php print t('Select Search Filters'); ?></h4>
  </div>
  <div class="col-wrapper clearfix">
    <?php if ($path == 'search/horses'): ?>
      <div class="col col-left">
        <div class="form-item form-item-checkboxes form-checkboxes-type clearfix">
          <?php print $widgets['filter-field_horse_adtype_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-checkboxes">
          <div class="label"><?php print $widgets['filter-field_horse_hunters_value_many_to_one']->label; ?></div>
          <?php print $widgets['filter-field_horse_hunters_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-checkboxes">
          <div class="label"><?php print $widgets['filter-field_horse_ponies_value_many_to_one']->label; ?></div>
          <?php print $widgets['filter-field_horse_ponies_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-center">
        <div class="form-item form-item-checkboxes">
          <div class="label"><?php print $widgets['filter-field_horse_jumpers_value_many_to_one']->label; ?></div>
          <?php print $widgets['filter-field_horse_jumpers_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-checkboxes">
          <div class="label"><?php print $widgets['filter-field_horse_equitation_value_many_to_one']->label; ?></div>
          <?php print $widgets['filter-field_horse_equitation_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-right">
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_price_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_location2_lid']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_sex_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_breed_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-name form-type-text">
          <div class="label"><?php print $widgets['filter-title']->label; ?></div>
          <?php print $widgets['filter-title']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Height Range'); ?></div>
          <?php print $widgets['filter-field_horse_height_value']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Age Range'); ?></div>
          <?php print $widgets['filter-field_horse_age_value']->widget; ?>
        </div>
      </div>
    <?php endif; ?><!-- /search/horses -->
    <?php if ($path == 'search/horses/hunters'): ?>
      <div class="col col-left">
        <div class="form-item form-item-checkboxes form-checkboxes-type form-clear-left clearfix">
          <?php print $widgets['filter-field_horse_adtype_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-checkboxes form-clear-bottom">
          <?php print $widgets['filter-field_horse_hunters_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-center">
        <div class="form-item form-item-dropdown form-add-top">
          <?php print $widgets['filter-field_horse_price_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_location2_lid']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_sex_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_breed_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-right">
        <div class="form-item form-type-text">
          <div class="label"><?php print $widgets['filter-title']->label; ?></div>
          <?php print $widgets['filter-title']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Height Range'); ?></div>
          <?php print $widgets['filter-field_horse_height_value']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Age Range'); ?></div>
          <?php print $widgets['filter-field_horse_age_value']->widget; ?>
        </div>
      </div>
    <?php endif; ?><!-- /search/horses/hunters -->
    <?php if ($path == 'search/horses/jumpers'): ?>
      <div class="col col-left">
        <div class="form-item form-item-checkboxes form-checkboxes-type form-clear-left clearfix">
          <?php print $widgets['filter-field_horse_adtype_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-checkboxes form-clear-bottom">
          <?php print $widgets['filter-field_horse_jumpers_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-center">
        <div class="form-item form-item-dropdown form-add-top">
          <?php print $widgets['filter-field_horse_price_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_location2_lid']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_sex_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_breed_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-right">
        <div class="form-item form-type-text">
          <div class="label"><?php print $widgets['filter-title']->label; ?></div>
          <?php print $widgets['filter-title']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Height Range'); ?></div>
          <?php print $widgets['filter-field_horse_height_value']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Age Range'); ?></div>
          <?php print $widgets['filter-field_horse_age_value']->widget; ?>
        </div>
      </div>
    <?php endif; ?><!-- /search/horses/jumpers -->
    <?php if ($path == 'search/horses/equitation'): ?>
      <div class="col col-left">
        <div class="form-item form-item-checkboxes form-checkboxes-type form-clear-left clearfix">
          <?php print $widgets['filter-field_horse_adtype_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-checkboxes form-clear-bottom">
          <?php print $widgets['filter-field_horse_equitation_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-center">
        <div class="form-item form-item-dropdown form-add-top">
          <?php print $widgets['filter-field_horse_price_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_location2_lid']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_sex_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_breed_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-right">
        <div class="form-item form-type-text">
          <div class="label"><?php print $widgets['filter-title']->label; ?></div>
          <?php print $widgets['filter-title']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Height Range'); ?></div>
          <?php print $widgets['filter-field_horse_height_value']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Age Range'); ?></div>
          <?php print $widgets['filter-field_horse_age_value']->widget; ?>
        </div>
      </div>
    <?php endif; ?><!-- /search/horses/equitation -->
    <?php if ($path == 'search/horses/ponies'): ?>
      <div class="col col-left">
        <div class="form-item form-item-checkboxes form-checkboxes-type form-clear-left clearfix">
          <?php print $widgets['filter-field_horse_adtype_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-checkboxes form-clear-bottom">
          <?php print $widgets['filter-field_horse_ponies_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-center">
        <div class="form-item form-item-dropdown form-add-top">
          <?php print $widgets['filter-field_horse_price_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_location2_lid']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_sex_value_many_to_one']->widget; ?>
        </div>
        <div class="form-item form-item-dropdown">
          <?php print $widgets['filter-field_horse_breed_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-right">
        <div class="form-item form-type-text">
          <div class="label"><?php print $widgets['filter-title']->label; ?></div>
          <?php print $widgets['filter-title']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Height Range'); ?></div>
          <?php print $widgets['filter-field_horse_height_value']->widget; ?>
        </div>
        <div class="form-item form-type-text">
          <div class="label"><?php print t('Age Range'); ?></div>
          <?php print $widgets['filter-field_horse_age_value']->widget; ?>
        </div>
      </div>
    <?php endif; ?><!-- /search/horses/ponies -->
  </div>
  <div class="col-bottom">
    <?php print $reset; ?>
    <?php print $button; ?>
  </div>
</div>