<div class="job-board-tabs quicktabs-style-proequest">
  <ul class="quicktabs-style-proequest">
    <li class="job-tab">
      <a href="/jobs">Jobs</a>
    </li>
    <li class="job-tab active">
      <a href="/node/add/job-posting">Post a Job</a>
    </li>
  </ul>
</div>
<div class="login-container">
  <h2 class="block-title">Login to Post a Job</h2>
  <?php    if ($intro_text): ?>
    <div id="ui-user-form-intro">
    <?php    print $intro_text; ?>
    </div>
  <?php    endif; ?>
  <div id="ui-user-form-wrapper">
    <?php    print $rendered; ?>
  </div>
  <div class="not-a-member login-block">
    <h2 class="block-title">Not a Member?</h2>
    <a class="button" href="/user/register">Join Now</a>
  </div>
  <div class="job-by-email login-block">
    <h2 class="block-title">Post a Job by E-mail</h2>
    <p>Email <a href="mailto:mattias@proequest.com">mattias@proequest.com</a> to have ProEquest post your job for you.</p>
    <p>Please supply all required information below</p>
    <ul>
      <li>Title</li>
      <li>Facility</li>
      <li>City</li
      ><li>State</li>
      <li>Full / Part Time</li>
      <li>Compensation</li>
      <li>Job Description</li>
      <li>Experience / Qualifications</li>
      <li>Email Contact</li>
      <li>Employer's Website</li>
    </ul>
  </div>
</div>
