<div class="form-filters-wrapper">
  <div class="col-top">
      <h4><?php print t('Select Search Filters'); ?></h4>
  </div>
  <div class="col-wrapper clearfix">
      <div class="col col-left">
        <div class="form-item form-item-dropdown">
          <div class="label"><?php print t('Discipline'); ?></div>
          <?php print $widgets['filter-field_profile_discipline_value_many_to_one']->widget; ?>
        </div>
      </div>
      <div class="col col-center">
        <div class="form-item form-item-dropdown">
          <div class="label"><?php print t('Location'); ?></div>
          <?php print $widgets['filter-field_profile_location_lid']->widget; ?>
        </div>
      </div>
      <div class="col col-right">
        <div class="form-item form-item-dropdown form-clear-top clearfix">
          <div class="label"><?php print t('Name'); ?></div>
          <?php print $widgets['filter-field_profile_search_value']->widget; ?>
        </div>
      </div>
  </div>
    <div class="col-bottom">
      <?php print $reset; ?>
      <?php print $button; ?>
    </div>
</div>