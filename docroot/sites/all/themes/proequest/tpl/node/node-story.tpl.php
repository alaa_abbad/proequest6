<?php
// $Id: node.tpl.php,v 1.1.2.3 2010/01/11 00:08:12 sociotech Exp $
$read_more = l("Read More", "node/" . $node->nid, array("attributes" => array("class" => "read-more-link")));
$photo_teaser = '<img src="/' . $node->field_teaser_image[0]['filepath']  . '" width="90" height="90"></img>';
$photo_full = '<img src="/' . $node->field_teaser_image[0]['filepath']  . '" ></img>';
$url = "node/" . $node->nid;
$host = strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'];

// The title has been turned off for the panel page that controls
// display of this node type (Title Type == No Title).
// So we load the node into another variable at no cost because it's cached.
$alt_node = node_load($node->nid);
if ($page) {
  proequest_set_og_metadata('og:title', $alt_node->title);
  proequest_set_og_metadata('og:type', 'article');
  if ($alt_node->field_teaser_image[0]['filepath']) {
    proequest_set_og_metadata('og:image', $host.'/'.$alt_node->field_teaser_image[0]['filepath']);
  }
}

?>

<div id="node-<?php print $node->nid; ?>" class="node <?php print $node_classes; ?>">
  <div class="inner">
    <?php print $picture ?>

    <?php if ($page == 0): ?>
    <h2 class="title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php endif; ?>

    <?php if ($byline): ?>
    <div class="meta">
      <span class="submitted">
        <?php print $byline; ?>
      </span>
      <?php if ($page) { ?>
      <div class="node-story-social">
        <div class="clearfix">
          <div class="social-button-wrapper"><?php print proequest_fb_recommend_button(false, $host.$node_url); ?></div>
          <div class="social-button-wrapper"><?php print proequest_tweet_button($host.$node_url, $title); ?></div>
        </div>
      </div>
      <?php } ?>
    </div>
    <?php endif; ?>

    <?php if ($submitted): ?>
    <div class="meta">
      <span class="submitted"><?php print $submitted ?></span>
    </div>
    <?php endif; ?>

    <?php if ($node_top && !$teaser): ?>
    <div id="node-top" class="node-top row nested">
      <div id="node-top-inner" class="node-top-inner inner">
        <?php print $node_top; ?>
      </div><!-- /node-top-inner -->
    </div><!-- /node-top -->
    <?php endif; ?>

    <div class="content clearfix">
      <div class="news-body">
      <?php if (!is_null($node->field_teaser_image[0]['filepath']) && ($page == 0)) : ?>
        <div class="news-image-teaser-container">
          <div class="news-image-teaser">
          <?php
          if ($page == 0) {
            print l($photo_teaser, $url, array('html' => true));
          }
          else {
            print l($photo_full, $url, array('html' => true));
          }
          ?>
          </div>
        </div> <!-- /news-image-teaser-container -->
      <?php endif ?>
      <?php
      if ($page == 0) {
        // Replace the tag with nothing
        $teaser = $alt_node->teaser;
        $teaser = preg_replace('/\[\[.*\]\]/', '', $teaser);
        print $teaser;
      }
      else {
        print $node->content['body']['#value'];
      }
      if ($page == 0) {
        print $read_more;
      ?>
        <div class="clearfix news-preview-buttons">
          <div class="social-button-wrapper"><?php print proequest_fb_recommend_button(false, $host.$node_url); ?></div>
          <div class="social-button-wrapper"><?php print proequest_tweet_button($host.$node_url, $title); ?></div>
        </div>
      <?php
      }
      ?>
      </div> <!-- /news-body -->
    </div>

  </div><!-- /inner -->

  <?php if ($node_bottom && !$teaser): ?>
  <div id="node-bottom" class="node-bottom row nested">
    <div id="node-bottom-inner" class="node-bottom-inner inner">
      <?php print $node_bottom; ?>
    </div><!-- /node-bottom-inner -->
  </div><!-- /node-bottom -->
  <?php endif; ?>
</div><!-- /node-<?php print $node->nid; ?> -->
<?php if ($page) { ?>
<div class="node-story-social">
  <p>&nbsp;</p>
  <hr class="news-hr-over-like" />
  <div class="clearfix">
    <div class="social-button-wrapper"><?php print proequest_fb_recommend_button(false, $host.$node_url); ?></div>
    <div class="social-button-wrapper"><?php print proequest_tweet_button($host.$node_url, $title); ?></div>
  </div>
  <?php print proequest_fb_comments($host.$node_url, 591); ?>
</div>
<?php } ?>
