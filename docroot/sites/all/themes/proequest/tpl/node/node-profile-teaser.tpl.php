<?php
/**
 * @file
 * Profile teaser template file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="node proequest-block <?php print $node_classes; ?>">
  <div class="node-wrapper">
    <div class="center clearfix">
      <div class="col-left">
        <div class="inner">
          <?php print $field_profile_picture_rendered; ?>
        </div>
      </div>
      <div class="col-center">
        <div class="inner">
          <h4><?php print l(html_entity_decode($title, ENT_QUOTES), 'node/' . $nid); ?></h4>
          <?php print $field_profile_barn_rendered; ?>
          <?php print $field_profile_discipline_rendered; ?>
          <?php print $field_profile_profession_rendered; ?>
        </div>
      </div>
      <div class="col-right">
        <div class="inner">
          <?php print $field_profile_location_rendered; ?>
        </div>
      </div>
    </div>
    <div class="bottom">
      <div class="inner clearfix">
        <?php print $field_profile_website_rendered; ?>
        <div class="social-links-wrapper">
          <?php print $field_profile_facebook_rendered; ?>
          <?php print $field_profile_twitter_rendered; ?>
          <?php print $field_profile_youtube_rendered; ?>
        </div>
      </div>
    </div>
  </div>
</div>