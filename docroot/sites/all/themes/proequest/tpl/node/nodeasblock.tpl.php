<?php
// $Id: nodeasblock.tpl.php,v 1.1.2.2 2009/09/17 12:40:36 herc Exp $

/**
 * @file nodeasblock.tpl.php
 * Default theme implementation for rendering a single node as a block.
 *
 * Available variables:
 * - $content: Node content rendered.
 * - $edit_link: Node edit link.
 *
 * @see template_preprocess_nodeasblock()
 */
?>
<?php print node_view(node_load($content), FALSE, TRUE, TRUE); ?>
<?php if (isset($edit_link)): ?>
<div class="nodeasblock-edit-link"><?php print $edit_link ?></div>
<?php endif; ?>
