<?php
  $node_title = $node->title;
  $disciplines_text = project6_horse_disciplines_list($node);
  $horse_sold_flag = flag_get_flag('sold');
  $horse_is_sold = $horse_sold_flag->is_flagged($node->nid);

  $horse_source = trim($node->field_horse_image[0]['filepath']);

  if ($horse_source ==  '') {
    $horse_source = '<img src="sites/all/themes/proequest/images/horse_profile.png" width="90" height="75">';

    $horse_image =  l($horse_source, 'node/' . $node->nid,
          array(
                'html' => TRUE
              )
          );
  }
  else {

    $horse_thumbnail_imagecache_preset = 'horse_thumbnail_professional';
    if ($horse_is_sold) {
      $horse_thumbnail_imagecache_preset = 'horse_thumbnail_professional_sold';
    }

    $horse_image =  l(
          theme('imagecache', $horse_thumbnail_imagecache_preset, $horse_source, $node_title, $node_title),
          'node/' . $node->nid,
          array(
                'html' => TRUE
              )
          );
  }

  $horse_location = project6_horse_location($node);
  $uid = $node->uid;

  $profile_node = content_profile_load('profile', $uid);

  $trimmed_pro = views_trim_text(array("max_length" => 20, "word_boundary" => true, "ellipsis" => true), $profile_node->title);
  $horse_professional = l($trimmed_pro, "node/" . $profile_node->nid);

  $view_horse = l('VIEW', "node/" . $node->nid, array('attributes' => array('class' => 'url-orange')));

?>

<div id="node-<?php print $node->nid; ?>" class="node professional-horses <?php print $node_classes; ?>">
  <div class="inner">


    <div class="content clearfix">
      <table class="table-professional-horses">
        <tr>
          <td class="image-cell">
            <?php print $horse_image; ?>
          </td>
          <td class="text-cell">
            <div class="horse-title"><a href="<?php print $node_url ?>" title="<?php print $node_title ?>"><?php print $node_title ?></a></div>
            <div class="horse-discipline">
              <?php print $disciplines_text; ?>
            </div>
            <div class="horse-location">
              <?php print $horse_location; ?>
            </div>
          </td>
          <td class="view-horse-cell">
            <?php print $view_horse ?>
          </td>
        </tr>
      </table>
    </div>

  </div><!-- /inner -->

</div><!-- /node-<?php print $node->nid; ?> -->
