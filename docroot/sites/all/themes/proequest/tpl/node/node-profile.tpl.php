<?php

//
// Full user profile page
//
global $user;

// This was asking for "global $user" but that fails when it's
// an administrator loking at the record. Needs to be the
// user being viewed.
$account = user_load($node->uid);
$profile_name = $node->field_profile_name[0]['first'] . " " . $node->field_profile_name[0]['last'];

// Check if administrator. Certain links need to be constructed differently
// to handle 'me'
$is_admin = false;
if (in_array('admin', $user->roles)) $is_admin = true;


$options['attributes'] = array('target' => '_blank');
$image_format = 'avatar_default';
if ($page == 0) $image_format = 'avatar_default';

// Don't show profile unless:
// - Is an admin
// - OR is owner of profile and is at least a site user (to allow renewals)
if ($page == 1) {
  $is_actual_user = ($account->uid == $user->uid);
  $has_site_role = in_array('Site User', $account->roles);
  if ((!$is_admin)&& (!$has_site_role && $is_actual_user)) {
    drupal_goto('professional-not-available');
  }
}

// Send to 'me' if module is enabled AND not the administrator viewing.
// The admin should be able to edit the profile and the current profile
// *not* be replaced by 'me.'
$me = $account->uid;
if (module_exists('me') && ($is_admin == false)) {
  $me = 'me';
}

$profile_barn_name    = content_format('field_profile_barn', $node->field_profile_barn[0]);
$profile_picture      = '<a href="' . $node_url . '">' . content_format('field_profile_picture', $node->field_profile_picture[0], $image_format) . '</a>';
$profile_disc_array   = content_format('field_profile_discipline', $node->field_profile_discipline[0]);
$profile_profession   = content_format('field_profile_profession', $node->field_profile_profession[0]);
$profile_bio          = content_format('field_profile_bio', $node->field_profile_bio[0]);

if ($node->field_profile_website[0]['url']) $profile_website      = l($node->field_profile_website[0]['url'], $node->field_profile_website[0]['url'], $options);

// link will contain HTML that must be passed through
$options['html'] = true;
if (isset($node->field_profile_youtube[0]['query'])) {
  $options['query'] = $node->field_profile_youtube[0]['query'];
}

if ($node->field_profile_facebook[0]['url']) $profile_facebook     = l('<img src="/_/content/facebook_icon_small.jpeg">', $node->field_profile_facebook[0]['url'], $options);

if ($node->field_profile_youtube[0]['url']) $profile_youtube      = l('<img src="/_/content/youtube_icon_small.png">', $node->field_profile_youtube[0]['url'], $options);

$profile_location     = content_format('field_profile_location', $node->field_profile_location[0]);

//	Contact this professional
//  Need the professional's email address for that
$subject = rawurlencode("ProEquest Inquiry");
$mailto = $account->mail . "?subject=" . $subject;


?>

<?php
if (($page == 1) && (isset($_REQUEST['destination']))):

	print '<p class="back-link">' . l(t('Back'), $_REQUEST['destination']) . '</p>';

endif;
?>

<div id="node-<?php print $node->nid; ?>" class="node proequest-block <?php print $node_classes; ?>">
  <div class="inner">

    <?php if ($page == 1): // full node ?>
        <h2 class="title"><?php print t('Professional Information'); ?></h2>
    <?php else:  ?>
      <h2 class="profile-professional-title"><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php endif; ?>

    <div class="profile-container">
      <div class="content profile-left-div">
        <?php print $profile_picture ?>
      </div>
      <div class="content profile-middle-div field">
        <table>
          <tr>
            <td width="100"><div class="field-label-inline-first">Barn Name:</div></td><td><?php print $profile_barn_name ?></td>
          </tr>
          <tr>
            <td><div class="field-label-inline-first">Discipline:</div></td><td><?php print $profile_disc_array ?></td>
          </tr>
          <tr>
            <td><div class="field-label-inline-first">Profession:</div></td><td><?php print $profile_profession ?></td>
          </tr>
          <?php
          if ($profile_website || $profile_facebook || $profile_youtube) $links = 'Links:';


          if ($profile_website): ?>
          <tr>
            <td><div class="field-label-inline-first"><?php if ($links) { print $links; $links = '';} ?></div></td><td><?php print $profile_website ?></td>
          </tr>
          <?php
          endif;

          if ($profile_facebook) {
            $both_social = $profile_facebook;
          }

          if ($profile_youtube) {
            $both_social .= $profile_youtube;
          }

          if ($both_social):
          ?>
          <tr>
            <td><div class="field-label-inline-first"><?php if ($links) { print $links; $links = '';} ?></div></td><td><?php print $both_social ?></td>
          </tr>
          <?php
          endif;
          ?>
          <tr>
            <td valign="top"><div class="field-label-inline-first">Location:</div></td><td><?php print $profile_location ?></td>
          </tr>
        </table>


        <?php

        if ($terms): ?><div class="terms"><?php print $terms; ?></div><?php endif;

        if ($links): ?><div class="links"><?php print $links; ?></div><?php endif;

        if ($page == 1):  // full node
          $adm_link = array();

          if (node_access('update', $node)) {
            $url_query = '';
            if (!$is_admin) $url_query = 'destination=user/me';
            $adm_link[] = '<span class="theme-button theme-button-dark">' .
                          l('<span><span>' . t('Edit My Profile') . '</span></span>', 'user/' . $me . '/profile/profile', array('html' => TRUE, 'query' => $url_query)) .
                          '</span>';
          }
          $a = theme('item_list', array_values($adm_link), NULL, 'ol', array('class' => 'adm-link clearfix'));

          print theme('item_list', array_values($adm_link), NULL, 'ol', array('class' => 'adm-link clearfix'));

  //        Allison requested that these be removed until payment can be established.
  //        $output = array();
  //        if (isset($node->field_sponsor_1[0]['filepath'])) {
  //          $img = theme('imagecache', 'sponsor', $node->field_sponsor_1[0]['filepath']);
  //          if (isset($node->field_sponsor_1_link[0]['value'])) {
  //            $output[] = l($img, $node->field_sponsor_1_link[0]['value'], array('html' => true, 'attributes' => array('target' => '_blank')));
  //          }
  //          else {
  //            $output[] = $img;
  //          }
  //        }
  //        if (isset($node->field_sponsor_2[0]['filepath'])) {
  //          $img = theme('imagecache', 'sponsor', $node->field_sponsor_2[0]['filepath']);
  //          if (isset($node->field_sponsor_2_link[0]['value'])) {
  //            $output[] = l($img, $node->field_sponsor_2_link[0]['value'], array('html' => true, 'attributes' => array('target' => '_blank')));
  //          }
  //          else {
  //            $output[] = $img;
  //          }
  //        }
  //        if (isset($node->field_sponsor_3[0]['filepath'])) {
  //          $img = theme('imagecache', 'sponsor', $node->field_sponsor_3[0]['filepath']);
  //          if (isset($node->field_sponsor_3_link[0]['value'])) {
  //            $output[] = l($img, $node->field_sponsor_3_link[0]['value'], array('html' => true, 'attributes' => array('target' => '_blank')));
  //          }
  //          else {
  //            $output[] = $img;
  //          }
  //        }
  //        if (isset($node->field_sponsor_4[0]['filepath'])) {
  //          $img = theme('imagecache', 'sponsor', $node->field_sponsor_4[0]['filepath']);
  //          if (isset($node->field_sponsor_4_link[0]['value'])) {
  //            $output[] = l($img, $node->field_sponsor_4_link[0]['value'], array('html' => true, 'attributes' => array('target' => '_blank')));
  //          }
  //          else {
  //            $output[] = $img;
  //          }
  //        }
  //        if (isset($node->field_sponsor_5[0]['filepath'])) {
  //          $img = theme('imagecache', 'sponsor', $node->field_sponsor_5[0]['filepath']);
  //          if (isset($node->field_sponsor_5_link[0]['value'])) {
  //            $output[] = l($img, $node->field_sponsor_5_link[0]['value'], array('html' => true, 'attributes' => array('target' => '_blank')));
  //          }
  //          else {
  //            $output[] = $img;
  //          }
  //        }
  //
  //        if (count($output)) {
  //          print '<p>&nbsp;</p><h2 class="AvenirRoman">Sponsors</h2>';
  //
  //          foreach ($output as $line) {
  //            print $line;
  //          }
  //        }


        endif; ?>

      </div> <!-- /middle div -->

      <div class="content profile-right-div field">
        <?php if ($page) print $profile_bio; ?>
      </div>

    </div> <!-- /container -->

  </div><!-- /inner -->
</div><!-- /node-<?php print $node->nid; ?> -->

<?php #dsm($node); ?>
