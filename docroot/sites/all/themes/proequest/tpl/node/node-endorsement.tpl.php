<?php
//  Endorsement
$field_endorsement_user = $node->field_endorsement_user[0]['nid'];
$field_endorsement_desc = $node->field_endorsement_desc[0]['value'];
$field_endorsement_home = $node->field_endorsement_home[0]['value'];
//  Profile
$featured               = node_load($field_endorsement_user);
$field_profile_nid      = $featured->nid;
$field_profile_name     = $featured->field_profile_name[0]['first'] . ' ' .
                          $featured->field_profile_name[0]['last'];
$field_profile_picture  = $featured->field_profile_picture[0]['filepath'];
$field_profile_location = $featured->field_profile_location[0]['city'] . ' ' .
                          $featured->field_profile_location[0]['province'] . ' ' .
                          $featured->field_profile_location[0]['postal_code'] . ' ' .
                          $featured->field_profile_location[0]['country_name'];

$read_more = l("Read More", "node/" . $node->nid, array("attributes" => array("class" => "read-more-link")));
$photo_teaser = '<img src="/' . $node->field_teaser_image[0]['filepath']  . '" width="90" height="90"></img>';
$photo_full = '<img src="/' . $node->field_teaser_image[0]['filepath']  . '" ></img>';
$url = "node/" . $node->nid;
$host = strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'];

$date_posted .= format_date($created, 'custom', 'F j, Y');
/*
 * For Facebook
 */

if ($page) {
  proequest_set_og_metadata('og:title', $alt_node->title);
  proequest_set_og_metadata('og:type', 'article');
  if ($alt_node->field_teaser_image[0]['filepath']) {
    proequest_set_og_metadata('og:image', $host.'/'.$alt_node->field_teaser_image[0]['filepath']);
  }
}

if ($field_profile_picture == '') {
  $field_profile_picture = '_/imagefield_default_images/default_avatar.png';
}

if ($page) {
  $link_to = 'node/' . $field_profile_nid;
}
else {
  $link_to = 'node/' . $node->nid;
}

?>
<div id="node-<?php print $node->nid; ?>" class="node <?php print $node_classes; ?>  clearfix">


    <?php
    // First handle the teaser

    if ($page == 0) { ?>

      <div class="node-endorsement-teaser-text">

        <h2 class="title"><?php print l('Featured Professional - '.$field_profile_name, $link_to, array('attributes' => array('title' => t('View profile')))); ?></h2>

		    <div class="meta">
		      <span class="submitted">
		        <?php print $date_posted; ?>
		      </span>
		    </div>

        <div class="news-image-teaser-container">
          <div class="news-image-teaser">
	        <?php
	          print l(theme('imagecache', 'avatar', $field_profile_picture, $field_profile_name, $field_profile_name), $link_to, array('html' => TRUE, 'attributes' => array('title' => t('View profile'))));
	          ?>
          </div>
	      </div>

<?php

// Shows location of professional
/*
        if ($field_profile_location != '') { ?>
          <address><?php print $field_profile_location; ?></address>
        <?php }
*/


        print "<p>" . $field_endorsement_home;
        print l(t('Read more'), 'node/' . $node->nid, array('attributes' => array('title' => t('Read the complete article about @name', array('@name' => $field_profile_name)), 'class' => 'read-more-link' )));
        print "</p>";


// Shows edit and view links for professional
        /*         if ($links): ?><div class="links"><?php print $links; ?></div><?php endif; */
	      /*
        if ($is_admin) {
          $adm_link = array();

          $adm_link[] = '<span class="theme-button theme-button-dark">' .
                        l('<span><span>' . t('Edit') . '</span></span>', 'admin/proequest/promoted',
                                array('html' => TRUE, 'attributes' => array('title' => t('Edit Promoted Professionals'), 'class' => 'adm-link-edit'))) . '</span>';

          print theme('item_list', array_values($adm_link), NULL, 'ol', array('class' => 'adm-link', 'style' => 'margin:1em 0;'));
        }
        */

?>

      <div class="featured-pro-social-wrapper">
        <div class="featured-pro-social">
          <div class="clearfix">
            <div class="social-button-wrapper"><?php print proequest_fb_recommend_button(false, $host.$node_url); ?></div>
            <div class="social-button-wrapper"><?php print proequest_tweet_button($host.$node_url, $title); ?></div>
          </div>
        </div>
        <!--
        <div class="featured-pro-view-buttons">
          <a class="button-small orange" href="/<?php print drupal_get_path_alias($link_to) ?>" id="button-view-now">VIEW PROFILE AND HORSES</a>
        </div>
        -->
      </div>




      </div>

    <?php
    } else {
      // Now the full page node
      ?>

      <?php
      if ($field_profile_location != ''): ?>
        <address><?php print $field_profile_location; ?></address>
      <?php
      endif;

      if ($field_profile_picture != ''):
        print l(theme('imagecache', 'avatar', $field_profile_picture, $field_profile_name, $field_profile_name), $link_to, array('html' => TRUE, 'attributes' => array('title' => t('View profile'))));
      endif;

      print $field_endorsement_desc;

      if ($links): ?><div class="links"><?php print $links; ?></div><?php endif;

      if ($is_admin) {
        $adm_link = array();

        $adm_link[] = '<span class="theme-button theme-button-dark">' .
                      l('<span><span>' . t('Edit') . '</span></span>', 'admin/proequest/promoted', array('html' => TRUE, 'attributes' => array('title' => t('Edit Promoted Professionals'), 'class' => 'adm-link-edit'))) .
                      '</span>';

        print theme('item_list', array_values($adm_link), NULL, 'ol', array('class' => 'adm-link', 'style' => 'margin:1em 0;'));
      }

      ?>

      <div class="featured-pro-social-wrapper">
        <div class="featured-pro-social">
          <div class="clearfix">
            <div class="social-button-wrapper"><?php print proequest_fb_recommend_button(false, $host.$node_url); ?></div>
            <div class="social-button-wrapper"><?php print proequest_tweet_button($host.$node_url, $title); ?></div>
          </div>
        </div>

        <div class="featured-pro-view-buttons">
          <a class="button-small orange" href="/<?php print drupal_get_path_alias($link_to) ?>" id="button-view-now">VIEW PROFILE AND HORSES</a>
        </div>
      </div>


    <?php
    }
    ?>

</div><!-- /node-<?php print $node->nid; ?> -->
