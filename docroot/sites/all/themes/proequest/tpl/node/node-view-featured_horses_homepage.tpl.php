<?php
  $node_title = $node->title;
  $disciplines_text = project6_horse_disciplines_list($node);

  if ($node->field_horse_image[0]['filepath'] != '') {

    $horse_thumbnail_imagecache_preset = 'horse_thumbnail_homepage';
    if ($field_horse_sold) {
      $horse_thumbnail_imagecache_preset = 'horse_thumbnail_sold';
    }

    $horse_image =  l(
          theme('imagecache', $horse_thumbnail_imagecache_preset, $node->field_horse_image[0]['filepath'], $node_title, $node_title),
          'node/' . $node->nid,
          array(
                'html' => TRUE
              )
          );
  }

  $horse_location = project6_horse_location($node);
  $uid = $node->uid;

  $profile_node = content_profile_load('profile', $uid);

  $horse_professional = l($profile_node->title, "node/" . $profile_node->nid);


?>

<div id="node-<?php print $node->nid; ?>" class="node <?php print $node_classes; ?>">
  <div class="inner">


    <div class="content clearfix">
      <table class="homepage-featured-horses">
        <tr>
          <td width="74">
            <?php print $horse_image; ?>
          </td><td>
            <div class="horse-title"><a href="<?php print $node_url ?>" title="<?php print $node_title ?>"><?php print $node_title ?></a></div>
            <div class="horse-discipline orange-text">
              <?php print $disciplines_text; ?>
            </div>
            <div class="horse-location">
              <?php print $horse_location; ?>
            </div>
            <div class="horse-professional">
              <?php print "PROFESSIONAL: $horse_professional" ?>
            </div>
          </td>
        </tr>
      </table>
    </div>

  </div><!-- /inner -->

</div><!-- /node-<?php print $node->nid; ?> -->
