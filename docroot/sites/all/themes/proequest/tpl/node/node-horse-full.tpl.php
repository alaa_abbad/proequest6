<?php
/**
 * @file
 * Horse template file
 */
$host = strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'];
?>
<div id="node-<?php print $node->nid; ?>" class="node proequest-block <?php print $node_classes; ?>">
  <div class="node-wrapper inner-row">
    <div class="center clearfix">
      <div class="col-left">
        <div class="col-inner">
          <h1 class="title"><?php print $title; ?></h1>
          <?php print proequest_render_fields($variables, array('adtype', 'hunters', 'jumpers', 'ponies', 'equitation', 'dressage', 'eventers')); ?>
          <div class="field-separator"></div>
          <?php print proequest_render_fields($variables, array('location', 'price'), 'small'); ?>
          <div class="field-separator"></div>
          <?php print proequest_render_fields($variables, array('age', 'height', 'sex', 'breed'), 'small'); ?>
          <div class="field-separator"></div>
          <?php print proequest_render_fields($variables, array('desc'), 'smaller'); ?>
          <?php print flag_create_link('featured', $node->nid); ?>
          <?php print flag_create_link('featured_hunters', $node->nid); ?>
          <?php print flag_create_link('featured_jumpers', $node->nid); ?>
          <?php print flag_create_link('featured_ponies', $node->nid); ?>
          <?php print flag_create_link('featured_equitation', $node->nid); ?>
          <?php print flag_create_link('featured_europe', $node->nid); ?>
          <?php if ($GLOBALS['user'] == $node->uid): ?>
            <?php print flag_create_link('sold', $node->nid); ?>
          <?php endif; ?>
        </div>
      </div>
      <div class="col-right">
        <div class="col-inner">
          <div class="field field-lead-image">
            <?php print theme('imagecache', 'horse_lead_image', $field_horse_image[0]['filepath']); ?>
          </div>
          <div class="field field-block field-photos">
            <div class="field-label"><?php print t('Photos'); ?></div>
            <?php print content_view_field(content_fields("field_horse_image"), $node, FALSE, FALSE); ?>
          </div>
          <div class="field field-block field-videos">
            <div class="field-label"><?php print t('Videos'); ?></div>
            <?php print content_view_field(content_fields("field_horse_video"), $node, FALSE, FALSE); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="bottom clearfix">
      <div class="col-left">
        <div class="col-inner">
          <div class="poster-profile">
  <?php
    $profile_picture = content_view_field(content_fields("field_profile_picture"), $profile, FALSE, FALSE);
    print l($profile_picture, 'node/' . $profile->nid, array('html' => TRUE));
   ?>

            <div class="field">
              <?php print l($profile->field_profile_name['0']['first'] . ' ' . $profile->field_profile_name['0']['last'], 'node/' . $profile->nid); ?>
            </div>
            <?php print content_view_field(content_fields("field_profile_location"), $profile, FALSE, FALSE); ?>
          </div>
        </div>
      </div>
      <div class="col-right">
        <div class="col-inner">
          <?php print $contact_button; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="node-share-links clearfix inner-row">
    <div class="social-button-wrapper"><?php print proequest_fb_recommend_button(); ?></div>
    <div class="social-button-wrapper"><?php print proequest_tweet_button($host.$node_url, $title); ?></div>
  </div>
  <div class="node-disclaimer inner-row">
    <p><strong><?php print ('Horse Listing Disclaimer'); ?></strong><br />
    <?php print t('ProEquest does not control, and is not responsible for, Content made available through the ProEquest.com site and Service, and, as such, does not guarantee or warrant any user or third party\'s products, horses, services, or representations. In particular, but without limitation, ProEquest is not responsible for the claims of any owner or seller regarding a horse(s) for sale or lease, including show records, health, performance, or fitness for a particular purpose. ProEquest strongly recommends that a pre-purchase exam be performed by the buyer\'s equine veterinarian of choice on any horse prior to purchase.'); ?></p>
  </div>
</div><!-- /node -->




