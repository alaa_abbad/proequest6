<div id="node-<?php print $node->nid; ?>" class="node <?php print $node_classes; ?> clearfix">
<?php	
	if ($node->field_cycle_image[0]['view'] != '') {
	
		$cycle = '';
		
		if (count($node->field_cycle_image) > 1) {
		}
		
			$cycle .= '<div id="pager-' . $node->nid . '"></div>';
			$cycle .= '<div id="slides-' . $node->nid . '">';
			
			foreach ($node->field_cycle_image as $field) {
				$cycle .= '<div class="slide clearfix">' . 
				
									theme('imagecache', 'cycle', $field['filepath'], $field['data']['alt'], ''); 
									
									if ($field['data']['title'] != '') {
										$cycle .= '<div class="slide-text">' . $field['data']['title'] . '</div>';
									}

									if ($field['data']['alt'] != '') {
										$cycle .= '<div class="slide-credits">' . $field['data']['alt'] . '</div>';
									}

				$cycle .= '</div>';
			}
			
			$cycle .= '</div>';
	
	}
	
	print '<div class="cycle clearfix">' . $cycle . '</div>';

#dsm($node);


if ($links): 
	print '<div class="links">' . $links . '</div>';
endif;
?>
</div><!-- /node-<?php print $node->nid; ?> -->
<script type="text/javascript">
$(document).ready(function(){
	$("#slides-<?php print $node->nid; ?>").cycle({
		fx:"fade",
		pager:"#pager",
		pause:1
		});
});
</script>