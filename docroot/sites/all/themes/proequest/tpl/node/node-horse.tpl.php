<?php
global $user;

//	Node
$node_nid					= $node->nid;
$node_title				= $node->title;
$node_name				= $node->name;
$node_realname		= $node->realname;
$node_uid					= $node->uid;

$is_admin = false;
if (in_array('admin', $user->roles)) $is_admin = true;


//	CCK Fields
$field_horse_price_view					= $node->field_horse_price[0]['view'];
$field_horse_hunters_view				= $node->field_horse_hunters;
$field_horse_jumpers_view				= $node->field_horse_jumpers;
$field_horse_equitation_view    = $node->field_horse_equitation;
$field_horse_ponies_view				= $node->field_horse_ponies;
$field_horse_eventer_view				= $node->field_horse_eventer;
$field_horse_dressage_view			= $node->field_horse_dressage;
$field_horse_breed_view					= $node->field_horse_breed[0]['view'];
$field_horse_age_view						= $node->field_horse_age[0]['view'];
$field_horse_sex_view						= $node->field_horse_sex[0]['view'];
$field_horse_desc_view					= $node->field_horse_desc[0]['view'];
$field_horse_height_view				= $node->field_horse_height[0]['view'];
$field_horse_adtype_array				= $node->field_horse_adtype;
$field_horse_discipline_array		= $node->field_horse_discipline;
$field_horse_image_array				= $node->field_horse_image;
$field_horse_video_array				= $node->field_horse_video;
//	Location
$field_horse_location_array			= $node->field_horse_location2;

$field_horse_sold               = FALSE;

$sold_flag = flag_get_flag('sold');
// Include isset($node->nid) because during preview this isn't set
// and thus the test passes otherwise
if ($sold_flag && isset($node->nid) && $sold_flag->is_flagged($node->nid)) {
  $field_horse_sold = TRUE;
}

if ($page == 1) {
  proequest_set_og_metadata('og:title', $node_title);
  proequest_set_og_metadata('og:type', 'athlete');
  proequest_set_og_metadata('og:description', $field_horse_desc_view);
  if ($field_horse_image_array) {
    $host = strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'].'/';
    foreach ($field_horse_image_array as $image_array) {
      if ($image_array['filepath']) {
        proequest_set_og_metadata('og:image', $host.$image_array['filepath']);
      }
    }
  }
}
?>

<div id="node-<?php print $node->nid; ?>" class="node proequest-block <?php print $node_classes; ?>">
  <div class="inner">


    <div class="content clearfix">
      <?php /*print $content*/ ?>

      <?php
        if (($page == 0) && ($field_horse_image_array[0]['filepath'] != '')) {

					$horse_thumbnail_imagecache_preset = 'horse_thumbnail';
					if ($field_horse_sold) {
						$horse_thumbnail_imagecache_preset = 'horse_thumbnail_sold';
					}

          print '<div class="horse-image">' .
								l(
                theme('imagecache', $horse_thumbnail_imagecache_preset, $field_horse_image_array[0]['filepath'], $node_title, $node_title),
								'node/' . $node_nid,
								array(
											'html' => TRUE,
											'attributes' => array(
																						'rel' => 'follow'
																						)
											)
								) .
                '</div>';
        }

			if ($page == 0) { ?>

			<h3 class="title"><?php print l($node_title, 'node/' . $node_nid); ?></h3>
			<?php } ?>

      <?php if ($page == 1) {
        // test if owner of this listing is valid member
        $seller = user_load($node_uid);
        if (!in_array('Professional', $seller->roles) && !$is_admin) {
          drupal_goto('horse-listing-not-available');
        }
      }
      ?>

      <div class="horse-main clearfix">
      	<div class="horse-details">
        	<ul class="clearfix">
          	<?php if (!empty($field_horse_adtype_array) && ($field_horse_adtype_array[0]['value'] != '')): ?>
          	<li class="clearfix"><span><?php print t('Ad type'); ?> </span><?php
							foreach ($field_horse_adtype_array as $adtype) {
								$adtype_text .= $adtype['view'] . ' &amp; ';
							}
							print substr($adtype_text, 0, -7);
						?></li>
            <?php endif; ?>

          	<?php if (!empty($field_horse_discipline_array)&& ($field_horse_discipline_array[0]['value'] != '')): ?>
          	<li class="clearfix"><span><?php print t('Discipline'); ?> </span><?php
							/* Update this code based on the values at http://www.proequest.com/admin/content/node-type/horse/fields/field_horse_discipline
							 * Possible values:
							 *
							 * Hunter
							 * Jumper
							 * Equitation
							 * Ponies
							 * Eventing
							 * Dressage
							 */
							foreach ($field_horse_discipline_array as $discipline) {
								$discipline_text .= $discipline['view'];

							 #if (($page == 1) && ($discipline['view'] == 'Hunter') && ($field_horse_hunters_view != '')) {
								if (($discipline['view'] == 'Hunter') && (!empty($field_horse_hunters_view))) {
                  $hunter_showing_in = '';
                  foreach ($field_horse_hunters_view as $hunter) {
                    $hunter_showing_in .= $hunter['view'] . ', ';
                  }
									if ($page == 1) {
										$discipline_text .= ' <em>' . substr($hunter_showing_in, 0, -2) . '</em>';
									}
								}

								if (($discipline['view'] == 'Jumper') && (!empty($field_horse_jumpers_view))) {
                  $jumpers_showing_in = '';
                  foreach ($field_horse_jumpers_view as $jumpers) {
                    $jumpers_showing_in .= $jumpers['view'] . ', ';
                  }
									if ($page == 1) {
                  	$discipline_text .= ' <em>' . substr($jumpers_showing_in, 0, -2) . '</em>';
									}
								}

								if (($discipline['view'] == 'Equitation') && (!empty($field_horse_equitation_view))) {
                  $equitation_showing_in = '';
                  foreach ($field_horse_equitation_view as $equitation) {
                    $equitation_showing_in .= $equitation['view'] . ', ';
                  }
									if ($page == 1) {
                  	$discipline_text .= ' <em>' . substr($equitation_showing_in, 0, -2) . '</em>';
									}
								}

								if (($discipline['view'] == 'Ponies') && (!empty($field_horse_ponies_view))) {
                  $ponies_showing_in = '';
                  foreach ($field_horse_ponies_view as $ponies) {
                    $ponies_showing_in .= $ponies['view'] . ', ';
                  }
									if ($page == 1) {
                  	$discipline_text .= ' <em>' . substr($ponies_showing_in, 0, -2) . '</em>';
									}
								}

								if (($discipline['view'] == 'Eventing') && (!empty($field_horse_eventer_view))) {
                  $eventer_showing_in = '';
                  foreach ($field_horse_eventer_view as $eventer) {
                    $eventer_showing_in .= $eventer['view'] . ', ';
                  }
									if ($page == 1) {
                  	$discipline_text .= ' <em>' . substr($eventer_showing_in, 0, -2) . '</em>';
									}
								}

								if (($discipline['view'] == 'Dressage') && (!empty($field_horse_dressage_view))) {
                  $dressage_showing_in = '';
                  foreach ($field_horse_dressage_view as $dressage) {
                    $dressage_showing_in .= $dressage['view'] . ', ';
                  }
									if ($page == 1) {
                  	$discipline_text .= ' <em>' . substr($dressage_showing_in, 0, -2) . '</em>';
									}
								}

								if ($page == 1) {
									$discipline_text .= '<br />';
								}
								else {
									$discipline_text .= ', ';
								}
							}

							if ($page == 1) {
								print substr($discipline_text, 0, -6);
							}
							else {
								print substr($discipline_text, 0, -2);
							}

						?></li>
            <?php endif; ?>

          	<?php if (!empty($field_horse_location_array)): ?>
          	<li class="clearfix"><span><?php print t('Location'); ?> </span><?php
							print $field_horse_location_array[0]['city'] . ', ' . $field_horse_location_array[0]['province'] . ' ' . $field_horse_location_array[0]['country_name'];
						?></li>
            <?php endif; ?>

          	<?php if (($page == 1) && ($field_horse_price_view != '')): ?>
          	<li class="clearfix"><span><?php print t('Price'); ?> </span><?php
							print $field_horse_price_view;
						?></li>
            <?php endif; ?>

          	<?php if (($page == 1) && ($field_horse_age_view != '')): ?>
          	<li class="clearfix"><span><?php print t('Year Born'); ?> </span><?php
							print $field_horse_age_view;
						?></li>
            <?php endif; ?>

          	<?php if (($page == 1) && ($field_horse_height_view != '')): ?>
          	<li class="clearfix"><span><?php print t('Height'); ?> </span><?php
							print $field_horse_height_view;
						?></li>
            <?php endif; ?>

          	<?php if (($page == 1) && ($field_horse_sex_view != '')): ?>
          	<li class="clearfix"><span><?php print t('Sex'); ?> </span><?php
							print $field_horse_sex_view;
						?></li>
            <?php endif; ?>

          	<?php if (($page == 1) && ($field_horse_breed_view != '')): ?>
          	<li class="clearfix"><span><?php print t('Breed'); ?> </span><?php
							print $field_horse_breed_view;
						?></li>
            <?php endif; ?>

          </ul>

          <?php
          if (in_array('admin', $user->roles)) {
            print flag_create_link('sold', $node->nid);
          }
          ?>

        </div>
        <?php
					if (($page == 1) && ( $field_horse_image_array[0]['filepath'] != '')) {

						$horse_imagecache_preset = 'horse';
						if ($field_horse_sold) {
							$horse_imagecache_preset = 'horse_sold';
						}
						print '<div class="horse-image">' .
									theme('imagecache', $horse_imagecache_preset, $field_horse_image_array[0]['filepath'], $node_title, $node_title) .
									'</div>';
					}
				?>
      </div>




    <?php if ($page == 1): ?>
						<?php if (!empty($field_horse_image_array) || !empty($field_horse_video_array)): ?>
            <div class="horse-media"><ul class="clearfix"><?php
              foreach ($field_horse_image_array as $image) {
                if ($image['view'] != '' ):
                  print '<li>' . $image['view'] . '</li>';
                endif;
              }
              foreach ($field_horse_video_array as $video) {
                if ($video['view'] != ''):
                  print '<li>' . $video['view'] . '</li>';
                endif;
              }
            ?></ul></div>
            <?php endif; ?>

            <div class="horse-info clearfix">
              <div class="horse-desc"><?php

                if ($field_horse_desc_view != ''):
                  print $field_horse_desc_view;
                endif;

              ?>
              <?php if ($terms): ?><div class="terms"><?php print $terms; ?></div><?php endif;?>

              <?php if ($links): ?><div class="links"><?php print $links; ?></div><?php endif; ?>

              <?php
              if (in_array('admin', $user->roles)) {
                print flag_create_link('sold', $node->nid);
              }
              ?>

              </div>






              <div class="horse-seller proequest-block">
                <div class="inner clearfix">
                  <h3 class="title"><?php print t('Seller Information'); ?></h3>
                  <div class="content clearfix">
                    <?php #print $submitted;
                    /*

                    $l_name = $seller->locations[0]['name'];
                    $l_city = $seller->locations[0]['city'];
										$l_prov = $seller->locations[0]['province_name'];
										$l_zip  = $seller->locations[0]['postal_code'];
										$l_ctry = $seller->locations[0]['country_name'];

										$seller_location = '';

										if ($l_city != '')		$seller_location .= $l_city . ', ';
										if ($l_prov != '')		$seller_location .= $l_prov . ' ';
										if ($l_zip  != '')		$seller_location .= $l_zip  . ' ';
										if ($l_ctry != '')		$seller_location .= '<br />' . $l_ctry;

										print ($l_name != '') ? '<strong>' . $l_name . '</strong><br />' : '';
										#print '<p>' . $seller_location .'</p>';
										*/




                    // Get all variables of the content profile of type 'profile'
                    $profile_var = $content_profile->get_variables('profile');

										//  Name / Picture (Links to profile page)
										$seller_img = '';
                    if ($profile_var['field_profile_picture'][0]['filepath'] != '') {
                      $seller_img .= theme('imagecache', 'avatar', $profile_var['field_profile_picture'][0]['filepath'], $profile_var['title'], '',  array('class' => 'seller'));
                    }
                    print '<h4 class="clearfix">' .
/* User's user page */   #l($seller_img . $profile_var['realname'], 'user/' . $profile_var['uid'], array('html' => TRUE, 'attributes' => array('title' => t('View seller\'s details')))) .
/* User's profile page */ l($seller_img . $profile_var['title'], 'node/' . $profile_var['nid'], array('html' => TRUE, 'attributes' => array('title' => t('View seller\'s complete profile')))) .
                          '</h4>';

										//	Contact Seller
                    //  Need the seller email address for that
                    $seller = user_load($node_uid);
                    $subject = rawurlencode("ProEquest Inquiry: " . $node->title);
                    $mailto = $seller->mail . "?subject=" . $subject;

                    print l(t("Contact the seller"), 'mailto:' . $mailto);
                    /* This was for the messaging system, which was broken
                     * Removed by AVA
                    if ($user->uid != $profile_var['uid']) {
                      print '<p>' .
                            l(t('Send a message to seller'), 'messages/new/' . $profile_var['uid'] . '/' . t('Message regarding') . ' ' . $node->title, array('html' => TRUE, 'query' => 'destination=node/' . $node->nid)) .
                            '</p>';
                    }
                    */

										//	Location
										$seller_loc = $profile_var['field_profile_location'];
										if (!empty($seller_loc[0])) {
											$l_str  = $seller_loc[0]['street'];
											$l_city = $seller_loc[0]['city'];
											$l_zip  = $seller_loc[0]['postal_code'];
											$l_prov = $seller_loc[0]['province_name'];
											$l_ctry = $seller_loc[0]['country_name'];
											$l_tel  = $seller_loc[0]['phone'];
											$l_fax  = $seller_loc[0]['fax'];
										}
										$seller_location = '';
										if ($l_str != '') {
											$seller_location .= '<span>' . $l_str 	. ' </span>';
										}
										if (($l_city != '') || ($l_zip != '') || ($l_prov != '')) {
											$seller_location .= '<span>' .
																					$l_city . ' ' .
																					$l_prov . ' ' .
																					$l_zip  . ' ' .
																					'</span>';
										}
										if ($l_ctry != '') {
											$seller_location .= '<span>' . $l_ctry . '</span>';
										}
										if ($seller_location != '') {
											print '<address class="seller-address">' . $seller_location . '</address>';
										}
										if ($l_tel != '') {
											print '<address class="seller-phone"><small>' . t('Phone')  . '</small> ' . $l_tel . '</address>';
										}
										if ($l_fax != '') {
											print '<address class="seller-fax"><small>' . t('Fax')  . '</small> ' . $l_fax . '</address>';
										}

/*
                    $seller_info = '';
                    //  Profession
                    if ($profile_var['field_profile_profession'][0]['safe'] != '') {
                      $seller_info .= '<li class="clearfix"><span>' . t('Profession') . ' </span>' . $profile_var['field_profile_profession'][0]['safe'] . '</li>';
                    }
                    //  Bio
                    #$profile_var['field_profile_bio']->safe
                    //  Discipline
                    if (!empty($profile_var['field_profile_discipline'])) {
                      $discipline_li = '';
                      foreach ($profile_var['field_profile_discipline'] as $d) {
                        if ($d['safe'] != '') {
                          $discipline_li .= $d['safe'] . ', ';
                        }
                      }
                      $seller_info .= '<li class="clearfix"><span>' . t('Discipline') . ' </span>' . substr($discipline_li, 0, -2) . '</li>';
                    }
                    if ($seller_info != '') {
                      print '<ul>' . $seller_info . '</ul>';
                    }

*/
?>
                  </div>
                </div>
              </div>

            </div>

            <div class="clearfix">
              <div class="social-button-wrapper"><?php print proequest_fb_recommend_button(); ?></div>
              <div class="social-button-wrapper"><?php print proequest_tweet_button(); ?></div>
            </div>

		<?php endif; ?>

    </div>

    <?php
		#if ($page == 1):
			$adm_link = array();

      // if (node_access('create', $node) && ($page == 1)) {
      //   $adm_link[] = '<span class="theme-button theme-button-dark">' .
      //                 l('<span><span>' . t('Post another horse') . '</span></span>', 'node/add/horse', array('html' => TRUE, 'attributes' => array('title' => t('Post another horse'), 'class' => 'adm-link-edit'))) .
      //                 '</span>';
      // }

      if (node_access('update', $node)) {
        $adm_link[] = '<span class="theme-button theme-button-dark">' .
                      l('<span><span>' . t('Edit') . '</span></span>', 'node/' . $node->nid . '/edit', array('html' => TRUE, 'attributes' => array('title' => t('Edit this'), 'class' => 'adm-link-edit'))) .
                      '</span>';
      }
			if (node_access('delete', $node)) {
				$adm_link[] = '<span class="theme-button">' .
											l('<span><span>' . t('Delete') . '</span></span>', 'node/' . $node->nid . '/delete', array('query' => 'destination=user/me', 'html' => TRUE, 'attributes' => array('title' => t('Delete this'), 'class' => 'adm-link-delete'))) .
											'</span>';
			}

			print theme('item_list', array_values($adm_link), NULL, 'ol', array('class' => 'adm-link'));


			//	Print SOLD? flag to node owners
			if ($node->uid == $user->uid) {
				print flag_create_link('sold', $node->nid);
			}



//			Print SOLD? editablefield checkbox:
//			if ($page == 1):
//				if (node_access('update', $node)) {
//					print $node->field_horse_sold[0]['view'];
//				}
//			endif;

		#endif;
		?>
  </div><!-- /inner -->
	<?php if ($page == 1): ?>
	<div id="node-disclaimer">
	<p><strong>Horse Listing Disclaimer</strong><br />
	ProEquest does not control, and is not responsible for, Content made available through the ProEquest.com site and Service, and, as such, does not guarantee or warrant any user or third party's products, horses, services, or representations. In particular, but without limitation, ProEquest is not responsible for the claims of any owner or seller regarding a horse(s) for sale or lease, including show records, health, performance, or fitness for a particular purpose. ProEquest strongly recommends that a pre-purchase exam be performed by the buyer's equine veterinarian of choice on any horse prior to purchase.</p>
	</div>
	<?php endif; ?>
</div><!-- /node-<?php print $node->nid; ?> -->




