<?php
// $Id: user-profile.tpl.php,v 1.2.2.2 2009/10/06 11:50:06 goba Exp $

/**
 * @file user-profile.tpl.php
 * Default theme implementation to present all user profile data.
 *
 * This template is used when viewing a registered member's profile page,
 * e.g., example.com/user/123. 123 being the users ID.
 *
 * By default, all user profile data is printed out with the $user_profile
 * variable. If there is a need to break it up you can use $profile instead.
 * It is keyed to the name of each category or other data attached to the
 * account. If it is a category it will contain all the profile items. By
 * default $profile['summary'] is provided which contains data on the user's
 * history. Other data can be included by modules. $profile['user_picture'] is
 * available by default showing the account picture.
 *
 * Also keep in mind that profile items and their categories can be defined by
 * site administrators. They are also available within $profile. For example,
 * if a site is configured with a category of "contact" with
 * fields for of addresses, phone numbers and other related info, then doing a
 * straight print of $profile['contact'] will output everything in the
 * category. This is useful for altering source order and adding custom
 * markup for the group.
 *
 * To check for all available data within $profile, use the code below.
 * @code
 *   print '<pre>'. check_plain(print_r($profile, 1)) .'</pre>';
 * @endcode
 *
 * Available variables:
 *   - $user_profile: All user profile data. Ready for print.
 *   - $profile: Keyed array of profile categories and their items or other data
 *     provided by modules.
 *
 * @see user-profile-category.tpl.php
 *   Where the html is handled for the group.
 * @see user-profile-item.tpl.php
 *   Where the html is handled for each item in the group.
 * @see template_preprocess_user_profile()
 */
?>
<?php #print $user_profile;
	
/*	
	$profile_var = $content_profile->get_variables('profile');
	//  Name / Picture (Links to profile page)
	$seller_img = '';
	if ($profile_var['field_profile_picture'][0]['filepath'] != '') {
		$seller_img .= theme('imagecache', 'avatar', $profile_var['field_profile_picture'][0]['filepath'], $profile_var['realname'], '',  array('class' => 'seller'));
	}
	
	#dsm($profile_var);
	
	print l($seller_img . $profile_var['realname'], 'node/' . $profile_var['nid'], array('html' => TRUE, 'attributes' => array('title' => t('View seller\'s complete profile'))));
	*/




/*
global $user;
	//	Load profile node
	$profile_var = content_profile_load('profile', $user->uid);
	
if (!empty($profile_var)) {
	return array(
		'latitude' => ((float) $profile_var['field_profile_location'][0]['latitude']),
		'longitude' => ((float) $profile_var['field_profile_location'][0]['longitude']),
	);
}
else {
	return array('latitude' =>0, 'longitude' => 0);
}

*/
?>



<div id="recently-posted-horses" class="proequest-block" style="float:none;">
<div id="recently-posted-horses-inner" class="recently-posted-horses-inner inner clearfix">
<?php print '<h2 class="AvenirRoman title">' . t('Recently Posted Horses') . '</h2>' . views_embed_view('recent_postings'); ?>
</div><!-- /recently-posted-horses-inner -->
</div><!-- /recently-posted-horses -->

<?php
#print theme('grid_block', '<h2 class="AvenirRoman title">' . t('Recently Posted Horses') . '</h2>' . views_embed_view('recent_postings'), 'proequest-block'); ?>

<p><?php print '<span class="theme-button theme-button-dark">' . 
					l('<span><span>' . t('Search for horses') . '</span></span>', 'search/horse', array('html' => TRUE, 'attributes' => array('title' => t('Search for horses')))) . 
					'</span>';
?></p>

