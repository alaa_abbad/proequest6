    <?php 
    $id = "filter-field_horse_search_value";
    $widget = $widgets[$id];
    ?>
    <div class="views-exposed-widget">
      <?php if (!empty($widget->label)): ?>
        <label for="<?php print $widget->id; ?>">
          <?php print $widget->label; ?>
        </label>
      <?php endif; ?>
      <?php if (!empty($widget->operator)): ?>
        <div class="views-operator">
          <?php print $widget->operator; ?>
        </div>
      <?php endif; ?>
      <div class="views-widget">
        <?php print $widget->widget; ?>
      </div>
    </div>
  
  
    <?php 
    $id = "filter-field_horse_breed_value_many_to_one";
    $widget = $widgets[$id];
    ?>
    <div class="views-exposed-widget">
      <?php if (!empty($widget->label)): ?>
        <label for="<?php print $widget->id; ?>">
          <?php print $widget->label; ?>
        </label>
      <?php endif; ?>
      <?php if (!empty($widget->operator)): ?>
        <div class="views-operator">
          <?php print $widget->operator; ?>
        </div>
      <?php endif; ?>
      <div class="views-widget">
        <?php print $widget->widget; ?>
      </div>
    </div>

    
    <?php 
    $id = "filter-field_horse_sex_value_many_to_one";
    $widget = $widgets[$id];
    ?>
    <div class="views-exposed-widget">
      <?php if (!empty($widget->label)): ?>
        <label for="<?php print $widget->id; ?>">
          <?php print $widget->label; ?>
        </label>
      <?php endif; ?>
      <?php if (!empty($widget->operator)): ?>
        <div class="views-operator">
          <?php print $widget->operator; ?>
        </div>
      <?php endif; ?>
      <div class="views-widget">
        <?php print $widget->widget; ?>
      </div>
    </div>

    
