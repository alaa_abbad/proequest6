<?php
// $Id: views-exposed-form.tpl.php,v 1.4.4.1 2009/11/18 20:37:58 merlinofchaos Exp $
/**
 * @file views-exposed-form.tpl.php
 *
 * This template handles the layout of the views exposed filter form.
 *
 * Variables available:
 * - $widgets: An array of exposed form widgets. Each widget contains:
 * - $widget->label: The visible label to print. May be optional.
 * - $widget->operator: The operator for the widget. May be optional.
 * - $widget->widget: The widget itself.
 * - $button: The submit button for the form.
 *
 * @ingroup views_templates
 */
?>
<div class="region-grey-gradient">
	<?php if (!empty($q)): ?>
		<?php
			// This ensures that, if clean URLs are off, the 'q' is added first so that
			// it shows up first in the URL.
			print $q;
		?>
	<?php endif; 
	
	// Determine what type of search we are to display
	$search_type = "All";
	switch (arg(2)) {
	case 'hunters':
		$search_type = "Hunters";
		break;
		
	case 'jumpers':
		$search_type = "Jumpers";
		break;
	
	case 'equitation':
		$search_type = "Equitation";
		break;
	
	case 'ponies':
		$search_type = "Ponies";
		break;
		
	} 
	
	?>
	<div class="views-exposed-form">
		<div class="views-exposed-widgets clear-block">
		
	
			<?php 
			$id = "filter-field_horse_search_value";
			$widget = $widgets[$id];
			?>
			<div class="views-exposed-widget">
				<?php if (!empty($widget->label)): ?>
					<label for="<?php print $widget->id; ?>">
						<?php print $widget->label; ?>
					</label>
				<?php endif; ?>
				<?php if (!empty($widget->operator)): ?>
					<div class="views-operator">
						<?php print $widget->operator; ?>
					</div>
				<?php endif; ?>
				<div class="views-widget">
					<?php print $widget->widget; ?>
				</div>
			</div>
	
			<?php 
			if ($search_type == "All" || $search_type == 'Hunters'):
				
				$id = "filter-field_horse_hunters_value_many_to_one";
				$widget = $widgets[$id];
				?>
				<div class="views-exposed-widget" id="filter-hunters">
					<?php if (!empty($widget->label)): ?>
						<label for="<?php print $widget->id; ?>">
							<?php print $widget->label; ?>
						</label>
					<?php endif; ?>
					<?php if (!empty($widget->operator)): ?>
						<div class="views-operator">
							<?php print $widget->operator; ?>
						</div>
					<?php endif; ?>
					<div class="views-widget">
						<?php print $widget->widget; ?>
					</div>
				</div>        
				<?php
			
			endif;
	
			if ($search_type == "All" || $search_type == 'Jumpers'):
				$id = "filter-field_horse_jumpers_value_many_to_one";
				$widget = $widgets[$id];
				?>
				<div class="views-exposed-widget" id="filter-jumpers">
					<?php if (!empty($widget->label)): ?>
						<label for="<?php print $widget->id; ?>">
							<?php print $widget->label; ?>
						</label>
					<?php endif; ?>
					<?php if (!empty($widget->operator)): ?>
						<div class="views-operator">
							<?php print $widget->operator; ?>
						</div>
					<?php endif; ?>
					<div class="views-widget">
						<?php print $widget->widget; ?>
					</div>
				</div>
				<?php 
			endif;
			
			if ($search_type == "All" || $search_type == 'Equitation'):
				$id = "filter-field_horse_equitation_value_many_to_one";
				$widget = $widgets[$id];
				?>
				<div class="views-exposed-widget" id="filter-equitation">
					<?php if (!empty($widget->label)): ?>
						<label for="<?php print $widget->id; ?>">
							<?php print $widget->label; ?>
						</label>
					<?php endif; ?>
					<?php if (!empty($widget->operator)): ?>
						<div class="views-operator">
							<?php print $widget->operator; ?>
						</div>
					<?php endif; ?>
					<div class="views-widget">
						<?php print $widget->widget; ?>
					</div>
				</div>
				<?php 
			endif;
				
			if ($search_type == "All" || $search_type == 'Ponies'):
				$id = "filter-field_horse_ponies_value_many_to_one";
				$widget = $widgets[$id];
				?>
				<div class="views-exposed-widget" id="filter-ponies">
					<?php if (!empty($widget->label)): ?>
						<label for="<?php print $widget->id; ?>">
							<?php print $widget->label; ?>
						</label>
					<?php endif; ?>
					<?php if (!empty($widget->operator)): ?>
						<div class="views-operator">
							<?php print $widget->operator; ?>
						</div>
					<?php endif; ?>
					<div class="views-widget">
						<?php print $widget->widget; ?>
					</div>
				</div>
				<?php 
			endif;
			
			$id = "filter-field_horse_adtype_value_many_to_one";
			$widget = $widgets[$id];
			?>
			<div class="views-exposed-widget" id="filter-adtype">
				<?php if (!empty($widget->label)): ?>
					<label for="<?php print $widget->id; ?>">
						<?php print $widget->label; ?>
					</label>
				<?php endif; ?>
				<?php if (!empty($widget->operator)): ?>
					<div class="views-operator">
						<?php print $widget->operator; ?>
					</div>
				<?php endif; ?>
				<div class="views-widget">
					<?php print $widget->widget; ?>
				</div>
			</div>
	
			
			<?php 
			$id = "filter-field_horse_price_value_many_to_one";
			$widget = $widgets[$id];
			?>
			<div class="views-exposed-widget" id="filter-price">
				<?php if (!empty($widget->label)): ?>
					<label for="<?php print $widget->id; ?>">
						<?php print $widget->label; ?>
					</label>
				<?php endif; ?>
				<?php if (!empty($widget->operator)): ?>
					<div class="views-operator">
						<?php print $widget->operator; ?>
					</div>
				<?php endif; ?>
				<div class="views-widget">
					<?php print $widget->widget; ?>
				</div>
			</div>
	
			<?php 
			$id = "filter-field_horse_location2_lid";
			$widget = $widgets[$id];
			?>
			<div class="views-exposed-widget">
				<?php if (!empty($widget->label)): ?>
					<label for="<?php print $widget->id; ?>">
						<?php print $widget->label; ?>
					</label>
				<?php endif; ?>
				<div class="views-widget">
					<?php print $widget->widget; ?>
				</div>
			</div>
	
	
			
			<?php 
			$id = "filter-field_horse_sex_value_many_to_one";
			$widget = $widgets[$id];
			?>
			<div class="views-exposed-widget">
				<?php if (!empty($widget->label)): ?>
					<label for="<?php print $widget->id; ?>">
						<?php print $widget->label; ?>
					</label>
				<?php endif; ?>
				<?php if (!empty($widget->operator)): ?>
					<div class="views-operator">
						<?php print $widget->operator; ?>
					</div>
				<?php endif; ?>
				<div class="views-widget">
					<?php print $widget->widget; ?>
				</div>
			</div>
			
			<?php 
			$id = "filter-field_horse_breed_value_many_to_one";
			$widget = $widgets[$id];
			?>
			<div class="views-exposed-widget">
				<?php if (!empty($widget->label)): ?>
					<label for="<?php print $widget->id; ?>">
						<?php print $widget->label; ?>
					</label>
				<?php endif; ?>
				<?php if (!empty($widget->operator)): ?>
					<div class="views-operator">
						<?php print $widget->operator; ?>
					</div>
				<?php endif; ?>
				<div class="views-widget">
					<?php print $widget->widget; ?>
				</div>
			</div>
	
				
			
			<div class="views-exposed-widget">
				<label>Age</label><br>
				<label for="edit-field-horse-age-value-min">From </label>
				<input type="text" maxlength="128" name="field_horse_age_value[min]" id="edit-field-horse-age-value-min" size="30" value="" class="form-text" />
				<label for="edit-field-horse-age-value-max">To </label>
				<input type="text" maxlength="128" name="field_horse_age_value[max]" id="edit-field-horse-age-value-max" size="30" value="" class="form-text" />
			</div>
						
		
			<div class="views-exposed-widget">
				<label>Height</label><br>
				<label for="edit-field-horse-height-value-min">From </label>
				<input type="text" maxlength="128" name="field_horse_height_value[min]" id="edit-field-horse-height-value-min" size="30" value="" class="form-text" />
				<label for="edit-field-horse-height-value-max">To </label>
				<input type="text" maxlength="128" name="field_horse_height_value[max]" id="edit-field-horse-height-value-max" size="30" value="" class="form-text" />
			</div>
	
			<div class="views-exposed-widget" id="horse-search-button">
				<?php print $button ?>
			</div>
		</div>
	</div>
</div>

