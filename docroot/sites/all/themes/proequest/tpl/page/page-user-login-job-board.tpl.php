<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">
<head>
  <?php print proequest_get_og_metadata(); ?>
  <?php print $head; ?>
  <title>Login to Post a Job | ProEquest</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <?php print $styles; ?>
  <link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/proequest/css/fonts.css?0" />
  <?php print $setting_styles; ?>
  <!--[if lt IE 7 ]> <?php print $ie6_styles; ?> <![endif]-->
  <!--[if IE 7]> <?php print $ie7_styles; ?> <![endif]-->
  <!--[if IE 8]> <?php print $ie8_styles; ?> <![endif]-->
  <!--[if IE 9]> <?php print $ie9_styles; ?> <![endif]-->
  <!--[if IE 7 ]>
  <link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/proequest/css/proequest-style-ie7.css" />
  <![endif]-->
  <!--[if IE 8 ]>
  <link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/proequest/css/proequest-style-ie8.css" />
  <![endif]-->
  <!--[if IE 9 ]>
  <link type="text/css" rel="stylesheet" media="all" href="/sites/all/themes/proequest/css/proequest-style-ie9.css" />
  <![endif]-->

  <?php print $local_styles; ?>
  <?php print $scripts; ?>
</head>
<!--[if lt IE 7 ]> <body id="<?php print $body_id; ?>" class="ie6 <?php print $body_classes; ?>"> <![endif]-->
<!--[if IE 7 ]>    <body id="<?php print $body_id; ?>" class="ie7 <?php print $body_classes; ?>"> <![endif]-->
<!--[if IE 8 ]>    <body id="<?php print $body_id; ?>" class="ie8 <?php print $body_classes; ?>"> <![endif]-->
<!--[if IE 9 ]>    <body id="<?php print $body_id; ?>" class="ie9 <?php print $body_classes; ?>"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <body id="<?php print $body_id; ?>" class="<?php print $body_classes; ?>"> <!--<![endif]-->
  <div id="page" class="page">
    <div id="page-inner" class="page-inner">
      <!-- header-top row: width = grid_width -->
      <?php print theme('grid_row', $header_top, 'header-top', 'full-width', $grid_width); ?>

      <!-- header-group row: width = grid_width -->
      <div id="header-group-wrapper" class="header-group-wrapper full-width">
        <div id="header-group" class="header-group row <?php print $grid_width; ?>">
          <div id="header-group-inner" class="header-group-inner inner clearfix">
            <?php print theme('grid_block', theme('links', $secondary_links), 'secondary-menu'); ?>
            <?php /*print theme('grid_block', $search_box, 'search-box');*/ ?>

            <div id="header-site-info" class="header-site-info block">
              <div id="header-site-info-inner" class="header-site-info-inner inner">
                <div id="logo">
                  <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><img src="/sites/all/themes/proequest/logo.png" alt="<?php print t('Home'); ?>" /></a>
                </div>
                <div id="header-badge">
                </div>
              </div><!-- /header-site-info-inner -->
            </div><!-- /header-site-info -->

            <!-- Place banners below -->

            <div class="header-spruce-meadows">
              <?php print $header_banner; ?>
            </div><!-- /header-spruce-meadows -->
            <?php print project6_welcome_user_menu(); ?>
            <?php print $header; ?>
          </div><!-- /header-group-inner -->
        </div><!-- /header-group -->
      </div><!-- /header-group-wrapper -->

      <div id="primary-menu-wrapper" class="primary-menu-wrapper full-width">
        <?php print theme('grid_row', $primary_links_tree, 'primary-menu', 'grid16-16', 'AvenirRoman'); ?>
      </div>


      <!-- preface-top row: width = grid_width -->
      <?php print theme('grid_row', $preface_top, 'preface-top', 'full-width', $grid_width); ?>

      <!-- main row: width = grid_width -->
      <div id="main-wrapper" class="main-wrapper full-width">
        <div id="main" class="main row <?php print $grid_width; ?>">
          <div id="main-inner" class="main-inner inner clearfix">


            <?php
            print theme('grid_row', $sidebar_first, 'sidebar-first', 'nested', $sidebar_first_width); ?>

            <!-- main group: width = grid_width - sidebar_first_width -->
            <div id="main-group" class="main-group row nested <?php print $main_group_width; ?>">
              <div id="main-group-inner" class="main-group-inner inner">
                <?php print theme('grid_row', $preface_bottom, 'preface-bottom', 'nested'); ?>

                <div id="main-content" class="main-content row nested">
                  <div id="main-content-inner" class="main-content-inner inner">
                    <!-- content group: width = grid_width - (sidebar_first_width + sidebar_last_width) -->
                    <div id="content-group" class="content-group row nested <?php print $content_group_width; ?>">
                      <div id="content-group-inner" class="content-group-inner inner">
                        <?php print theme('grid_block', $breadcrumb, 'breadcrumbs'); ?>

                        <?php if ($content_top || $help || $messages): ?>
                        <div id="content-top" class="content-top row nested">
                          <div id="content-top-inner" class="content-top-inner inner">
                            <?php
                            print theme('grid_block', $help, 'content-help');
                            print theme('grid_block', $messages, 'content-messages');
                            print $content_top;
                            ?>
                          </div><!-- /content-top-inner -->
                        </div><!-- /content-top -->
                        <?php endif; ?>

                        <div id="content-region" class="content-region row nested">
                          <div id="content-region-inner" class="content-region-inner inner">
                            <?php
                            if (($user->uid > 0) && (in_array('admin', $user->roles) || ($user->uid == 1))) {
                              print theme('grid_block', $tabs, 'content-tabs');
                            }
                            ?>
                            <div id="content-inner" class="content-inner block">
                              <div id="content-inner-inner" class="content-inner-inner inner"><?php

                            if ($profile_title == ''):
                            if ($title): ?><h1 class="pagetitle"><?php

                                    if ((arg(0) == 'user') && (arg(1) == 'register') && (arg(2) != '')) {
                                        if (arg(2) == 'professional'):
                                          print 'Professionals Register Now!';
                                        endif;
                                        if (arg(2) == 'amateur'):
                                          print 'Amateurs Register Now!';
                                        endif;
                                        if (arg(2) == 'vendor'):
                                          print 'Vendors Register Now!';
                                        endif;
                                    }
                                    else {
                                      print $title;
                                    }

                                  ?></h1><?php
                                  endif;
                            endif;
                            ?><?php

                            ?><div id="content-content" class="content-content">
                                        <?php print $content; ?>
                                  <?php #print $feed_icons; ?>
                                </div><!-- /content-content -->
                                <?php /*endif;*/ ?>
                              </div><!-- /content-inner-inner -->
                            </div><!-- /content-inner -->
                          </div><!-- /content-region-inner -->
                        </div><!-- /content-region -->

                        <?php print theme('grid_row', $content_bottom, 'content-bottom', 'nested'); ?>
                      </div><!-- /content-group-inner -->
                    </div><!-- /content-group -->

                    <div class="panel-col-last panel-panel">
                      <?php
                        $block = module_invoke('panels_mini', 'block', 'view', 'news_ad_collection');
                        print $block['content'];
                      ?>
                    </div>
                  </div><!-- /main-content-inner -->
                </div><!-- /main-content -->

                <?php print theme('grid_row', $postscript_top, 'postscript-top', 'nested'); ?>
              </div><!-- /main-group-inner -->
            </div><!-- /main-group -->

          </div><!-- /main-inner -->
        </div><!-- /main -->
      </div><!-- /main-wrapper -->

      <!-- postscript-bottom row: width = grid_width -->
      <?php print theme('grid_row', $postscript_bottom, 'postscript-bottom', 'full-width', $grid_width); ?>

      <!-- footer-message row: width = grid_width -->
      <div id="footer-message-wrapper" class="footer-message-wrapper full-width">
        <div id="footer-message" class="footer-message row <?php print $grid_width; ?>">
          <div id="footer-message-inner" class="footer-message-inner inner clearfix">
            <div id="footer-logo">
              <a href="<?php print check_url($front_page); ?>" title="<?php print t('Home'); ?>"><img src="/sites/all/themes/proequest/footer-logo.png" alt="<?php print t('ProEquest'); ?>" /></a>
              <p><?php print t('!year all rights reserved', array('!year' => '&copy;' . date('Y'))); ?></p>
            </div>
            <div id="footer">
              <?php print $footer ?>
            </div>
            <div id="footer-social-icons">
              <h4><?php print t('Connect'); ?><h4>
              <a href="https://www.facebook.com/ProEquest" class="icon-facebook" target="_blank"><?php print t('Facebook'); ?></a>
              <a href="http://twitter.com/#!/proequest" class="icon-twitter" target="_blank"><?php print t('Twitter'); ?></a>
              <a href="http://instagram.com/proequestcommunity" class="icon-instagram" target="_blank"><?php print t('Instagram'); ?></a>
            </div>
          </div><!-- /footer-message-inner -->
        </div><!-- /footer-message -->
      </div><!-- /footer-message-wrapper -->

    </div><!-- /page-inner -->
  </div><!-- /page -->
  <div class="hidden" style="display:none;">
    <div style="background-image:url(/sites/all/themes/proequest/images/bg-user-menu-hover.png);"></div>
  </div>
  <?php print $closure; ?>

<?php /*
<script type="text/javascript" src="//smarticon.geotrust.com/si.js"></script>
*/ ?>
</body>
</html>
<!-- <?php print $GLOBALS['base_url']; ?> -->
