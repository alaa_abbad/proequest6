<div id="ui-user-form-intro" class="clearfix"><?php
  
  $item = array();

if ((arg(0) == 'user') && (arg(1) == 'register') && (arg(2) != '')) {
  if (arg(2) == 'professional'):
#    $item[] = t('Are you a Vendor? Sign up on <a href="@vendor_registration">Vendors registration page</a> instead.', array('@vendor_registration' => url('user/register/vendor')));
#    $item[] = t('Amateurs, sign up on <a href="@amateur_registration">amateurs registration page</a>.', array('@amateur_registration' => url('user/register/amateur')));
  endif;
  if (arg(2) == 'amateur'):
    $item[] = t('Are you a Professional? Sign up on <a href="@pro_registration">professionals registration page</a> instead.', array('@pro_registration' => url('user/register/professional')));
    $item[] = t('Vendors, sign up on <a href="@vendor_registration">vendors registration page</a> instead.', array('@vendor_registration' => url('user/register/vendor')));
  endif;
  if (arg(2) == 'vendor'):
    $item[] = t('Are you a Professional? Sign up on <a href="@pro_registration">professionals registration page</a> instead.', array('@pro_registration' => url('user/register/professional')));
    $item[] = t('Amateurs, sign up on <a href="@amateur_registration">amateurs registration page</a>.', array('@amateur_registration' => url('user/register/amateur')));
  endif;
}
  $item[] = t('Find out more about available <a href="@membership">ProEquest membership plans</a>. Still have questions? <a href="@contact">Contact us</a>.', array('@membership' => url('node/19'), '@contact' => url('node/46')));

  print theme('item_list', array_values($item), NULL, 'ul', array('class' => 'registration-copy clearfix'));

?></div>
<div id="ui-user-form-wrapper" class="clearfix"><?php print $rendered; ?></div>
