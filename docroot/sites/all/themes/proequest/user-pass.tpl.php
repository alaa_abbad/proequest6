<?php    if ($intro_text): ?>
  <div id="ui-user-form-intro">
  <?php    print $intro_text; ?>
  </div>
<?php    endif; ?>
  <div id="ui-user-form-wrapper">
    <?php    print $rendered; ?>
  </div>
