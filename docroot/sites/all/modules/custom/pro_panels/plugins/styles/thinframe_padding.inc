<?php

/**
 * @file
 * Definition of the 'thin frame' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Thin frame with padding'),
  'description' => t('Display the pane with a thin frame around it and adding around content.'),
  'render pane' => 'panels_thin_frame_padding_style_render_pane',
  'weight' => 0,
);


/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_thin_frame_padding_style_render_pane($vars) {
  $class = 'panel-thin-frame-padding ';

  if (isset($vars->css_class))  $class .= $vars->css_class;


  $output = '<div class="' . $class . '"';
  if (isset($vars->css_id)) $output .= ' id="' . $vars->css_id . '"';
  $output .= '>';
  if ($vars->title) $output .= '<h2 class="pane-small-title">' . $vars->title . '</h2>';
  $output .= $vars->content;
  $output .= '</div>';
  return $output;
}

