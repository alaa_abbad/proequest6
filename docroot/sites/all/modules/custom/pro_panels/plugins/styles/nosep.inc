<?php
// $Id: default.inc,v 1.1.2.8 2010/07/13 23:55:58 merlinofchaos Exp $

/**
 * @file
 * Definition of the 'no separator' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('No Separators'),
  'description' => t('Display the region with no separators.'),
  'render region' => 'panels_nosep_style_render_region',
  'weight' => -15,
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_nosep_style_render_region($display, $region_id, $panes, $settings) {
  $output = '<div class="panel-nosep">';

  foreach ($panes as $pane_id => $pane_output) {
    // AVA: Remove separators
    //    if ($print_separator) {
    //      $output .= '<div class="panel-region-separator"></div>';
    //    }
    $output .= $pane_output;
//    $output .= panels_render_pane($pane_output, $display->content[$pane_id], $display);
  }
  $output .= "</div>";

  return $output;
}
