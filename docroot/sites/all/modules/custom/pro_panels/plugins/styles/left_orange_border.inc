<?php
// $Id: default.inc,v 1.1.2.8 2010/07/13 23:55:58 merlinofchaos Exp $

/**
 * @file
 * Definition of the 'left orange border' panel style.
 */

// Plugin definition
$plugin = array(
  'title' => t('Left Orange Border'),
  'description' => t('Display the region with a thin orange frame on the left side.'),
  'render region' => 'panels_left_orange_border_style_render_region',
  'weight' => -15,
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_panels_left_orange_border_style_render_region($display, $region_id, $panes, $settings) {
  $output = '<div class="panel-left-orange-border">';

  $print_separator = FALSE;
  foreach ($panes as $pane_id => $pane_output) {
    // AVA: Remove separators
    //    if ($print_separator) {
    //      $output .= '<div class="panel-region-separator"></div>';
    //    }

    $output .= $pane_output;
    // If we displayed a pane, this will become true; if not, it will become
    // false.
    $print_separator = (bool) $pane_output;
  }
  $output .= "</div>";

  return $output;
}
