<?php
// $Id: Exp $

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Vendor node logo (left col)'),
  'icon' => 'icon_node.png',
  'description' => t('Show logo for vendor.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_vendor_node_logo_content_type_render($subtype, $conf, $panel_args, $context) {

  $cr = chr(10) . chr(13);
  $nid = $panel_args[0];

  $node = node_load($nid);

  // Set link options for vendor logos
  $options['attributes'] = array('target' => '_blank');
  $options['html'] = TRUE;

  $vendor_logo = theme('imagecache', 'vendor_detail_logo', $node->field_vendor_logo[0]['filepath']);

  $block = new stdClass();
  $block->title = '';

  $block->content = '<div class="vendor-node-logo-container">';
  $block->content .= '<div class="vendor-node-logo"><table class="logo-table"><tr><td class="logo-table-cell">' . $vendor_logo . '</table></tr></td></div>' . $cr;
  $block->content .= '</div>';

  return $block;
}
