<?php
/**
 * Plugins definition
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Member : Profile Block'),
  'icon' => 'icon_node.png',
  'description' => t('Member profile block'),
  'render callback' => 'pro_panels_member_profile_block_content_type_render',
  'category' => t('ProEquest'),
  'single' => TRUE,
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Render the custom content type.
 */
function pro_panels_member_profile_block_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $uid = $args[0];
  $profile = content_profile_load('profile', $uid);

  $block->title = t('Welcome @name', array('@name' => $profile->title));
  $block->content = '<h2 class="pane-title">' . t('My Profile') . ' &raquo;</h2>';

  $block->content .= '<div class="profile-photo">';
  $block->content .= theme('imagecache', 'profile_125x125', $profile->field_profile_picture[0]['filepath']);
  $block->content .= '</div>';

  $block->content .= '<div class="fields-wrapper">';

  $block->content .= '<div class="field">';
  $block->content .= '<div class="field-label">';
  $block->content .= t('Name');
  $block->content .= '</div>';
  $block->content .= '<div class="field-value">';
  $block->content .= $profile->title;
  $block->content .= '</div>';
  $block->content .= '</div>';

  $block->content .= '<div class="field">';
  $block->content .= '<div class="field-label">';
  $block->content .= t('Barn');
  $block->content .= '</div>';
  $block->content .= '<div class="field-value">';
  $block->content .= $profile->field_profile_barn[0]['value'];
  $block->content .= '</div>';
  $block->content .= '</div>';

  $block->content .= '<div class="field">';
  $block->content .= '<div class="field-label">';
  $block->content .= t('Location');
  $block->content .= '</div>';
  $block->content .= '<div class="field-value">';
  $block->content .= $profile->field_profile_location2[0]['value'];
  $block->content .= '</div>';
  $block->content .= '</div>';

  $block->content .= '<div class="field">';
  $block->content .= '<div class="field-label">';
  $block->content .= t('Discipline');
  $block->content .= '</div>';
  $block->content .= '<div class="field-value">';
  $block->content .= $profile->field_profile_discipline[0]['value'];
  $block->content .= '</div>';
  $block->content .= '</div>';

  $options['attributes'] = array('class' => 'btn-edit');
  $options['query'] = 'destination=user/me';

  $block->content .= l(t('Edit'), 'user/me/profile/profile', $options);
  $block->content .= l(t('Preview'), 'node/' . $profile->nid, array('attributes' => array('class' => 'btn-preview')));

  $block->content .= '</div>';

  return $block;
}

