<?php
/**
 * Plugins definition
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Member : My Account Menu'),
  'icon' => 'icon_node.png',
  'description' => t('My Account menu'),
  'render callback' => 'pro_panels_member_my_account_menu_content_type_render',
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_member_my_account_menu_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = l(t('My Account') . ' &raquo;', 'user/me/edit', array('html' => true));
  // $block->title = t('My Account') . ' &raquo;';

  $links = array(
    // 'user/me/postings' => t('My Postings'),
    'user/me/member-info' => t('Membership'),
    'user/me/order-history' => t('Order History'),
    'user/me/edit/ms_core_billing_info' => t('Billing Info'),
    //'user/me/profile/profile' => t('Edit Profile'),
  );

  $block->content = '<ul class="menu">';
  foreach ($links as $path => $title) {
    $block->content .= '<li>';
    $block->content .= l($title, $path);
    $block->content .= '</li>';
  }
  $block->content .= '</ul>';

  return $block;
}

