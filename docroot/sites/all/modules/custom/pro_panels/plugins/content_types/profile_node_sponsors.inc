<?php
// $Id: Exp $

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Professional Profile Node Sponsor'),
  'icon' => 'icon_node.png',
  'description' => t('Shows up on right side of professional profile.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_profile_node_sponsors_content_type_render($subtype, $conf, $panel_args, $context) {
  global $user;

  $cr = chr(10) . chr(13);
  $nid = $panel_args[0];

  $node = node_load($nid);
  $account = user_load($node->uid);

  $pro_profile = content_profile_load('profile', $account->uid);
  $first_name = $node->field_profile_name[0]['first'] . " Recommends:";

  // Set link options for vendor logos
  $options['attributes'] = array('target' => '_blank');
  $options['html'] = TRUE;

  // Load vendor profile
  foreach ($pro_profile->field_vendors as $vendor) {
    if (isset($vendor['nid'])) {
      $vendor_profile = node_load($vendor['nid']);
      $vfp = $vendor_profile->field_vendor_logo[0]['filepath'];
      $vlogo = theme('imagecache', 'vendor_logo_sidebar', $vfp);
      $vlink  = $vendor_profile->field_vendor_homepage[0]['url'];
      $vendor_others_logo[] = l($vlogo, $vlink, $options);
    }
  }

  if (count($vendor_others_logo) == 0) $first_name = "Featured Vendors:";

  $block = new stdClass();
  $block->title = '';

  $block->content .= '<div class="profile-sidebar">' . $cr;

  // Show vendors/sponsors
  $block->content .= <<<EOD
  <div class="vendor-others">
    <div class="vendor-irecommend">
      <h1 class="font-dearjoe orange-text irecommend-name">$first_name</h1>
    </div>
EOD;

  if (count($vendor_others_logo) > 0) {
    $block->content .= '<div class="vendor-all-logos">' . $cr;
    // Show vendor logos
    foreach ($vendor_others_logo as $vendor_logo) {
      $block->content .= '  <div class="vendor-other-logo">' . $cr;
      $block->content .= "  " . $vendor_logo . $cr;
      $block->content .= '  </div>' . $cr;
    }
    $block->content .= '</div> <!-- end of vendor-all-logos -->' . $cr;
  }
  else {
    $panel_mini = panels_mini_load('proequest_recommends');
    $block->content .= panels_render_display($panel_mini->display);
  }
  $block->content .= '  </div> <!--  end of vendor-others div -->' . $cr;

  $block->content .= '</div> <!--  end of profile-sidebar div -->' . $cr;

  return $block;
}
