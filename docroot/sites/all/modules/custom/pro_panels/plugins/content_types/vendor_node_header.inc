<?php
// $Id: Exp $

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Vendor Node Header'),
  'icon' => 'icon_node.png',
  'description' => t('Header for vendor detail page.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_vendor_node_header_content_type_render($subtype, $conf, $panel_args, $context) {
  $cr = chr(10) . chr(13);

  $nid = $panel_args[0];
  if (isset($_SESSION['vendor_category'])) {
    $vendor_category_short = $_SESSION['vendor_category']['short name'];
    $vendor_category_full = $_SESSION['vendor_category']['full name'];
  }
  else {
    // User has come directly to the page without going through
    // vendor landing page. Find first category vendor belongs to.
    $vendor_category_short = 'feed';
    $vendor_category_full = 'Feed';
  }
  $node = node_load($nid);

  $options['attributes'] = array('target' => '_blank');

  $vendor_category_link = l($vendor_category_full, "vendors/" . $vendor_category_short);
  $browse_link = l("Browse A-List", 'vendors');


//  $vendor_homepage = l( 'www.cavalor.com', 'http://www.cavalor.com', $options);

	/* 	Get the url value from the field "field_vendor_hompage" */
	$vendor_url = $node->field_vendor_homepage[0]['url'];
	/* 	Remove the http:// and https:// from the string */
	$vendor_url_clean = preg_replace('#^https?://#', '', $vendor_url);
	/* 	Out put as a link to the vendor homepage and add the attribute options to the link */
	$vendor_homepage = l($vendor_url_clean, $node->field_vendor_homepage[0]['url'], $options);


  $node = node_load($nid);

  // Isolate the term
  $term_name = arg(1);
  $term_obj = taxonomy_get_term_by_name($term_name);
  $term_values = term_fields_get_fields_values($term_obj);

  // Prepare profile image
  $header_path = $node->field_vendor_header_image[0]['filepath'];

  $block = new stdClass();
  // Need to set title for browser page title but hide it on panel via CSS
  $block->title = '';

  $block->content .= <<<EOD
  <div class="vendor-header"  style="background: url(/$header_path)">
        <div class="vendor-header-text-background gradient">
          <div class="vendor-header-left-nav">
            <span class="more-link">&laquo;</span>&nbsp;$browse_link &nbsp;&nbsp;&nbsp;<span class="more-link">&laquo;</span>&nbsp;$vendor_category_link
          </div>
          <div class="vendor-header-link-to-vendor">
            $vendor_homepage
          </div>
        </div>
  </div> <!--  end of vendor-header -->

EOD;

  return $block;
}

