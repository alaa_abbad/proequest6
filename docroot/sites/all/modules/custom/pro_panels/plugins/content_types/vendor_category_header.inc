<?php
// $Id: Exp $

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Vendor Category Page Header'),
  'icon' => 'icon_node.png',
  'description' => t('Header for list of vendors by category.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_vendor_category_header_content_type_render($subtype, $conf, $panel_args, $context) {

  $cr = chr(10) . chr(13);

  $options['attributes'] = array('target' => '_blank');

  // Isolate the term
  $term_name = arg(1);
  $term_obj = taxonomy_get_term_by_name($term_name);
  $term_values = term_fields_get_fields_values($term_obj[0]);
  $term_name = $term_values["name_value"];
  $term_desc = array_pop($term_obj)->description;

  // If description is longer than 75 characters, make sure it moves to next line
  if (strlen($term_desc) > 100) {
    $break = strpos($term_desc, ' ', 75);
    $p1 = substr($term_desc, 0, $break);
    $p2 = substr($term_desc, $break + 1);
    $new_desc = $p1 . "</br>" . $p2;
  } else {
    $new_desc = $term_desc;
  }



  // Save the category for the detail page
  $_SESSION['vendor_category'] = array('short name' => str_replace(' ', '-', strtolower($term_values['name_value'])), 'full name' => $term_name);

  // Prepare profile image
  $header_path = "/sites/all/themes/proequest/images/VendorCategory" . rawurlencode($term_name) .  ".jpg";

  $block = new stdClass();
  // Need to set title for browser page title but hide it on panel via CSS
  $block->title = '';

  $block->content .= <<<EOD
  <div class="vendor-category-header" style="background: url($header_path)">
    <div class="vendor-category-text-background">
      <div class="vendor-category-text">
        $new_desc
      </div>
    </div>
  </div> <!--  end of vendor-category-header -->
EOD;

  return $block;
}



