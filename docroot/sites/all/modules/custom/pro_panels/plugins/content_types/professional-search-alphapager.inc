<?php
/**
 * Plugins definition
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Professional Search Alphabetic Pager'),
  'icon' => 'icon_node.png',
  'description' => t('Alphabetic Pager for professional search.'),
  'render callback' => 'pro_panels_professional_search_alphapager_content_type_render',
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_professional_search_alphapager_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = '';

  $pager = '<ul class="alpha-pager clearfix">';
  $chars = str_split("abcdefghijklmnopqrstuvwxyz");
  foreach ($chars as $char) {
    $active = '';
    
    // set query
    $query = array('alpha' => $char);
    if ($_GET['alpha'] == $char) {
      $active = "active";
    }

    $pager .= '<li class="pager-item ' . $active . '">';
    $pager .= l($char, $_GET['q'], array('query' => $query));
    $pager .= '</li>';
  }
  $pager .= '</ul>';

  // set the pager only when no filter is set
  if (!isset($_GET['query'])) {
    $block->content = $pager;
  }

  return $block;
}

