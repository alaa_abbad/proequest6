<?php
/**
 * Plugins definition
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Professional Search Tabs'),
  'icon' => 'icon_node.png',
  'description' => t('Tabs for professional search.'),
  'render callback' => 'pro_panels_professional_search_tabs_content_type_render',
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_professional_search_tabs_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = '';

  $tabs = array(
    'search/professionals' => t('All'),
    'search/professionals/trainers' => t('Trainers'),
    'search/professionals/vendors' => t('Vendors'),
    'search/professionals/riders' => t('Riders'),
    'search/professionals/breeders' => t('Breeders'),
    'search/professionals/facilities' => t('Facilities'),
  );

  $block->content = '<ul class="quicktabs-style-proequest">';
  foreach ($tabs as $path => $title) {
    $block->content .= '<li>';
    $block->content .= l($title, $path);
    $block->content .= '</li>';
  }
  $block->content .= '</ul>';

  return $block;
}

