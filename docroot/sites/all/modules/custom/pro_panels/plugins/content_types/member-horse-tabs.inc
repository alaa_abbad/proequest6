<?php
/**
 * Plugins definition
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Horse Search Tabs'),
  'icon' => 'icon_node.png',
  'description' => t('Tabs for horse search.'),
  'render callback' => 'pro_panels_member_horse_tabs_content_type_render',
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_member_horse_tabs_content_type_render($subtype, $conf, $args, $context) {
  $block = new stdClass();
  $uid = $args[0];
  $horses = proequest_library_get_user_horses($uid);
  $flagged = proequest_library_get_user_flagged_nodes($uid, 'favorites');
  $path = strtolower($_GET['q']);

  if (drupal_match_path($path, "user/*")) {
    $block->title = t('Sales Horses');
  }

  $tabs = array(
    'user/me' => t('My Horses !count', array('!count' => '<span>(' . sizeof($horses) . ')</span>')),
/*     'user/me/favourites' => t('My Favorites !count', array('!count' => '<span>(' . sizeof($flagged) . ')</span>')), */
  );

  $block->content = '<ul class="quicktabs-style-proequest">';
  foreach ($tabs as $path => $title) {
    $block->content .= '<li>';
    $block->content .= l($title, $path, array('html' => TRUE));
    $block->content .= '</li>';
  }
  $block->content .= '</ul>';

  $block->content .= l(t('Add Horse !icon', array('!icon' => '<i class="icon"></i>')), 'node/add/horse', array(
    'html' => TRUE,
    'attributes' => array('class' => 'btn-add-horse'),
  ));

  return $block;
}

