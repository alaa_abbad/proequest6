<?php
// $Id: Exp $

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Professional Profile Node Body'),
  'icon' => 'icon_node.png',
  'description' => t('Body for professional profile.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_profile_node_body_content_type_render($subtype, $conf, $panel_args, $context) {
  global $user, $base_url;

  $cr = chr(10) . chr(13);

  $nid = $panel_args[0];

  $node = node_load($nid);
  $account = user_load($node->uid);

  $options['attributes'] = array('target' => '_blank');

  $profile_name = $node->field_profile_name[0]['first'] . " " . $node->field_profile_name[0]['last'];
  $profile_barn_name    = content_format('field_profile_barn', $node->field_profile_barn[0]);

  // Prepare profile image
  $picture_path = $node->field_profile_picture[0]['filepath'];
  $profile_picture = '';
  if (trim($picture_path) != '') {
    $profile_picture .= theme('imagecache', 'avatar', $picture_path);
  } else {
    $field = content_fields('field_profile_picture', 'profile');
    $profile_picture = theme('imagecache', 'profile_80x80', $field['widget']['default_image']['filepath']);
  }

  $profile_disc_array = array();
  foreach ($node->field_profile_discipline as $discipline) {
    $profile_disc_array[] = content_format('field_profile_discipline', $discipline);
  }
  $profile_discipline = implode(', ', $profile_disc_array);

  $profile_profession_array = array();
  print content_format($node->field_profile_profession[0]);
  foreach ($node->field_profile_profession as $profession) {
    $profile_profession_array[] = content_format('field_profile_profession', $profession);
  }
  $profile_profession = implode(', ', $profile_profession_array);


  // Prepare profile bio
  $bio = trim(strip_all($node->field_profile_bio[0]['value']));
  $profile_bio = '';
  if ($bio != '') {
    $profile_bio = '<div class="profile-bio">About:<br>' . $bio . '</div>';
  }

  // Prepare website link
  $turl = strtolower($node->field_profile_website[0]['url']);
  // Want no http for text, but definitely http for actual url
  if ($turl) {
    $url = ((substr($turl, 0, 7) == 'http://') ? '' : 'http://') . $turl;
    $text_url = (substr($turl, 0, 7) == 'http://') ? substr($turl, 7) : $turl;
    $profile_website      = '<span class="url-orange">' . l($text_url, $url, $options) . '</span>';
  }

  // link will contain HTML that must be passed through
  $options['html'] = TRUE;

  // Prepare FB link
  $turl = strtolower($node->field_profile_facebook[0]['url']);
  if ($turl) {
    $url = ((substr($turl, 0, 8) == 'https://') ? '' : 'https://') . $turl;
    $profile_facebook     = '<div class="profile-social-icon facebook">' . l('<img src="/sites/all/themes/proequest/images/facebook_profile.png">', $url, $options) . '</div>';
  }

  // Prepare YouTube link
  $turl = $node->field_profile_youtube[0]['url'];
  if ($turl) {
    $url = ((substr($turl, 0, 7) == 'http://') ? '' : 'http://') . $turl;
    $profile_youtube      = '<div class="profile-social-icon youtube">' . l('<img src="/sites/all/themes/proequest/images/youtube_profile.png">', $url, $options) . '</div>';
  }

  // Barn pictures in colorbox
  foreach ($node->field_barn_pictures as $pic) {
    $path = $pic['filepath'];
    if (is_null($path)) break;
    $pictures .=  content_format("field_barn_pictures", $pic, "barn_picture_thumbnails__colorbox");

  }

  // Prepare unsold listings
  $viewName = "profile_horse_postings";
  $display_id = "panel_pane_1";
  $args = $node->uid;

  $horse_unsold_out = views_embed_view($viewName, $display_id, $args);
  $horse_unsold_count = strpos($horse_unsold_out, "img src");



  // Prepare sold listings
  $viewName = "profile_horse_postings";
  $display_id = "panel_pane_2";
  $args = $node->uid;
  $horse_sold_out = views_embed_view($viewName, $display_id, $args);
  $horse_sold_count = strpos($horse_sold_out, "img src");

  if (($horse_sold_count) || ($horse_unsold_count)) {
    $horse_title = $node->field_profile_name[0]['first'] . "'s Sale Horses";
  }

  // For location field
  $location = $node->field_profile_location[0];
  $profile_location      = "<dl>";
  $profile_location     .= '<dd>' . $location['street'] . '</dd>' . $cr;
  $profile_location     .= '<dd>' . $location['additional'] . '</dd>' . $cr;
  $citystate            = ($location['city'] ? $location['city'] .', ' : '') . ($location['province'] ? $location['province'] .'   ' : '') . $location['postal_code'];

  if ($citystate)
    $profile_location .= '<dd>' . $citystate . '</dd>' . $cr;

  if ($location['country'])
    $profile_location .=  '<dd>' . $location['country_name'] . '</dd>' . $cr;

  $maplink = location_map_link($location, '');
  if ($maplink) $profile_location .= '<dd class="url-orange">' . $maplink  . '</dd>' . $cr;
  $profile_location     .= "</dl>";

  $profile_contact_number = $location['phone'];

  //	Contact this professional
  //  Need the professional's email address for that
  $subject = rawurlencode("ProEquest Inquiry");
  $mailto = $node->field_contact_email[0]['value'] . "?subject=" . $subject;
  $attribs = array('attributes' => array('class' => 'button-medium-ish orange'));
  $profile_contact_professional = l(t("CONTACT"), 'mailto:' . $mailto, $attribs);

  proequest_set_og_metadata('og:title', $profile_name);
  proequest_set_og_metadata('og:image',  $base_url . '/' . $node->field_profile_picture[0]['filepath']);
  proequest_set_og_metadata('og:type', 'athlete');

  $profile_fb = proequest_fb_like_button(false);

  $block = new stdClass();
  // Need to set title for browser page title but hide it on panel via CSS
  $block->title = $profile_name;

  $block->content .= <<<EOD
  <div id="node-$node->nid" class="node proequest-block $node_classes">
    <div class="inner">
      <div class="profile-container">
        <div class="content profile-left-div">
        $profile_picture
        $profile_fb
        </div> <!--  end of profile-left-div -->

        <div class="content profile-middle-div field">
          <h2 class="title">$profile_name</h2>
          <table>
            <tr>
              <td width="110">Barn Name:</td><td>$profile_barn_name</td>
            </tr>
            <tr>
              <td>Discipline:</td><td>$profile_discipline</td>
            </tr>
            <tr>
              <td>Profession:</td><td>$profile_profession</td>
            </tr>
EOD;
            if ($profile_website) {
              $block->content .= <<<EOD
              <tr>
                <td>Website:</td><td>$profile_website</td>
              </tr>
EOD;
            }


            $block->content .= <<<EOD
            <tr>
              <td class="table-extra-top-margin-small">Barn Location:</td><td class="table-extra-top-margin-small">$profile_location</td>
            </tr>
EOD;
          if ($profile_contact_number) {
              $block->content .= <<<EOD
              <tr>
                <td class="table-extra-top-margin-small">Contact Number:</td><td class="table-extra-top-margin-small">$profile_contact_number</td>
              </tr>
EOD;
            }

          $block->content .= "</table>";

          if ($terms) $block->content .= '<div class="terms">' . $terms . '</div>' . $cr;
          if ($links) $block->content .= '<div class="terms">' . $links . '</div>' . $cr;



          $block->content .= <<<EOD
          </div> <!--  end of profile-middle-div -->
          <div class="content profile-right-div field">
EOD;
            if ($profile_facebook || $profile_youtube) {
              $block->content .= '<div class="profile-social"><div class="profile-connect">Connect with them:</div>' . $cr;

              // Center icons within
              $block->content .= '<div class="profile-both-icons">';

              if ($profile_facebook) $block->content .= $profile_facebook;

              if ($profile_youtube) $block->content .= $profile_youtube;

              $block->content .= '</div> <!-- End of profile-both-icons --> ' . $cr;
              $block->content .= '</div> <!-- End of profile-social --> ' . $cr;

            }

          $block->content .= <<<EOD
          </div> <!--  end of profile-right-div -->
EOD;

        $block->content .= <<<EOD
      </div> <!--  end of profile-container -->
EOD;
        $block->content .= $profile_bio . $cr;

        $block->content .= '<div class="button-contact-professional">' . $profile_contact_professional . '</div>';

        $block->content .= <<<EOD
    </div> <!--  end of inner -->
  </div> <!--  end of node div -->

EOD;

  // Print barn pictures if any exist
  if ($pictures) {
    $block->content .= '<h2 class="title my-barn">My Barn</h2>' . $cr;
    $block->content .= '<div class="barn-pictures">' . $cr;
    $block->content .= $pictures . $cr;

    $block->content .= '</div>';
  }

  // Show professional's horses in two groups. First the unsold ones
  if ($horse_title) {
    $block->content .= '<h2 class="title horse-title">' . $horse_title . '</h2>' . $cr;
  }


  if ($horse_unsold_out) {
    $block->content .= '<div class="professional-horses-wrapper">' . $cr;
    $block->content .= $horse_unsold_out . $cr;
    $block->content .= '</div>';
  }

  // Now the sold ones
  if ($horse_sold_out) {
    $block->content .= '<div class="professional-horses-wrapper">' . $cr;
    $block->content .= $horse_sold_out . $cr;
    $block->content .= "</div>";
  }

  return $block;
}

function strip_all($str) {
  $str = trim(preg_replace('/<[^>]*>/', '', $str));
  return $str;
}