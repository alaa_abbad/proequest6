<?php
// $Id: Exp $

/**
 * This is in use.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Vendor Node List of Professionals (Actual)'),
  'icon' => 'icon_node.png',
  'description' => t('Show featured professionals as chosen by administrator.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_vendor_node_pros_content_type_render($subtype, $conf, $panel_args, $context) {
  $host = strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'];

  $cr = chr(10) . chr(13);
  $nid = $panel_args[0];


  // Set link options for vendor logos
  $options['attributes'] = array('target' => '_blank');
  $options['html'] = TRUE;

  $node = node_load($nid);
  if (isset($node->field_professional_1[0]['nid'])) $values[] = $node->field_professional_1[0]['nid'];
  if (isset($node->field_professional_2[0]['nid'])) $values[] = $node->field_professional_2[0]['nid'];
  if (isset($node->field_professional_3[0]['nid'])) $values[] = $node->field_professional_3[0]['nid'];
  if (isset($node->field_professional_4[0]['nid'])) $values[] = $node->field_professional_4[0]['nid'];
  if (isset($node->field_professional_5[0]['nid'])) $values[] = $node->field_professional_5[0]['nid'];

  if (count($values) == 0) { return; }

  $query = "
    SELECT DISTINCT node.nid AS nid,
    ctp.field_profile_barn_value,
    ctp.field_profile_location_lid,
    ctp.field_profile_name_last, ctp.field_profile_name_first,
    files.filepath as filepath
    FROM node node
    LEFT JOIN content_type_profile ctp ON node.nid = ctp.nid AND node.vid = ctp.vid
    LEFT JOIN files files ON files.fid = ctp.field_profile_picture_fid
    WHERE (node.type in ('profile')) AND (node.status = 1)
    AND node.nid in (%s)
  ";

  $nids = implode(",", $values);

  $result = db_query($query, $nids);

  // Set up query to obtain the discipline
  $query = "
  SELECT field_profile_discipline_value
  FROM content_field_profile_discipline cfpd
  WHERE cfpd.nid = %d
  ";


  $block = new stdClass();
  $block->title = 'Featured Professionals' . $cr;


  while ($row = db_fetch_object($result)) {
    // Set up the picture
    $file =  $row->filepath;

    $attributes = array();

    $pic = l(theme("imagecache", 'avatar', $file, "", "", $attributes), "node/" . $row->nid, array('html' => TRUE));

    // Create the name
    $name = l($row->field_profile_name_first . " " . $row->field_profile_name_last, 'node/' . $row->nid);
    $barn = trim($row->field_profile_barn_value);


    // Fetch the profile discipline
    $p_values[0] = $row->nid;
    $profile_result = db_query($query, $p_values);
    $p_row = db_fetch_object($profile_result);
    $discipline = $p_row->field_profile_discipline_value;

    // Fetch the address
    $a_values[0] = $row->field_profile_location_lid;
    $address_result = db_query("SELECT * FROM location WHERE lid = %d", $a_values);
    $a_row = db_fetch_object($address_result);
    $address = '';
    if (strtoupper($a_row->country) == "US") {
      // For U.S. include the state but omit the country
      if (trim($a_row->city) <> '') {
          $address .= $a_row->city . ", " . $a_row->province;
      }
      else {
        $address = $a_row->province;
      }
    } else {
      // For non-U.S. don't include the state but include the country
      if (trim($a_row->city) <> '') {
        $address .= $a_row->city . ", " . strtoupper($a_row->country);
      }
      else {
        $address = strtoupper($a_row->country);
      }

    }

    $content .= '<div class="vendor-node-pro">' . $cr;
    $content .= '<div class="vendor-node-pro-pic">' . $pic . '</div>' . $cr;
    $content .= '<div class="vendor-node-pro-name ">' . $name . '</div>' . $cr;

    if ($address) {
      $content .= '<div class="vendor-node-pro-address">' . $address . '</div>' . $cr;
    }
    if ($barn) {
      $content .= '<div class="vendor-node-pro-barn">Barn: ' . $barn . '</div>' . $cr;
    }

    $content .= '<div class="vendor-node-pro-discipline"> Discipline: ' . $discipline . '</div>' . $cr;
    $content .= '</div>' . $cr;

  }

  $block->content = $content;

  return $block;
}
