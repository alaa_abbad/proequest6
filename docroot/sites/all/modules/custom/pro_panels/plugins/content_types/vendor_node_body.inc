<?php
// $Id: Exp $

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Vendor Node Body'),
  'icon' => 'icon_node.png',
  'description' => t('Content for vendor node.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_vendor_node_body_content_type_render($subtype, $conf, $panel_args, $context) {
  global $base_url;
  $host = strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'];

  $cr = chr(10) . chr(13);
  $nid = $panel_args[0];

  $node = node_load($nid);

  proequest_set_og_metadata('fb:app_id', '279646535422557');
  proequest_set_og_metadata('og:image', imagecache_create_url('vendor_detail_logo', $node->field_vendor_logo[0]['filepath']));
  proequest_set_og_metadata('og:title', $node->title);
proequest_set_og_metadata('og:type', "company");
  proequest_set_og_metadata('og:description', $node->body);

  // Set link options for vendor logos
  $options['attributes'] = array('target' => '_blank');
  $options['html'] = TRUE;

  $vendor_fb = proequest_fb_like_button();
  $vendor_tweet = proequest_tweet_button($base_url . url('node/' . $node->nid), $node->title);

  $vendor_homepage = l($node->field_vendor_homepage[0]['url'], $node->field_vendor_homepage[0]['url'], $options);

  // Prepare unsold listings
  $viewName = "vendor_products";
  $display_id = "panel_pane_1";
  $args = $node->nid;
  $product_view = views_embed_view($viewName, $display_id, $args);

  $vendor_desc = check_markup($node->body);

  $block = new stdClass();
  $block->title = '<h1 class="vendor-node-title">' . $node->title . '</h1>' . $cr;

  $block->content .= <<<EOD
  <div class="vendor-node-description">
    $vendor_desc
  </div>
  <div class="vendor-product-matrix-box pane-rounded pane-background-taupe">
EOD;


  $block->content .= '<h2 class="vendor-featured-products-title">Featured Products</h2>' . $cr;
  $block->content .= '<div class="vendor-featured-product-matrix">' . $product_view . "</div>" . $cr;
  $block->content .= '<div class="clearboth">&nbsp;</div>';
  $block->content .= '</div>' . $cr;
  $block->content .= '</div>' . $cr;

  $block->content .= $vendor_fb . $vendor_tweet;


  return $block;
}
