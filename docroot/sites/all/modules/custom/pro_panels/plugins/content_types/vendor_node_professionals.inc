<?php
// $Id: Exp $

/**
 * Not used...for prosperity.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Vendor Node List of Professionals (not used)'),
  'icon' => 'icon_node.png',
  'description' => t('Show professionals who have selected this vendor.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_vendor_node_professionals_content_type_render($subtype, $conf, $panel_args, $context) {
  $host = strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'];

  $cr = chr(10) . chr(13);
  $nid = $panel_args[0];


  // Set link options for vendor logos
  $options['attributes'] = array('target' => '_blank');
  $options['html'] = TRUE;

  // Fetch pros selected to be featured.
  $query = "
      SELECT DISTINCT node.nid AS nid,
      ctp.field_profile_barn_value,
      ctp.field_profile_location_lid,
      ctp.field_profile_name_last, ctp.field_profile_name_first,
      files.filepath as filepath
      FROM node node
      LEFT JOIN content_field_vendors cfv ON node.nid = cfv.nid AND node.vid = cfv.vid
      LEFT JOIN content_type_profile ctp ON node.nid = ctp.nid AND node.vid = ctp.vid
      LEFT OUTER JOIN files files ON files.fid = ctp.field_profile_picture_fid
      WHERE (node.type in ('profile')) AND (node.status = 1)
      AND cfv.field_vendors_nid = %d
    ";

  $values[] = $nid;

  $result = db_query($query, $values);

  $query = "
    SELECT field_profile_discipline_value
    FROM content_field_profile_discipline cfpd
    WHERE cfpd.nid = %d
    ";

  $block = new stdClass();
  $block->title = 'Featured Professionals' . $cr;

  $items = array();
  while ($row = db_fetch_object($result)) {
    // Set up the picture
    $file =  $row->filepath;

    $attributes = array();

    $pic = l(theme("imagecache", 'avatar', $file, "", "", $attributes), "node/" . $row->nid, array('html' => TRUE));

    // Create the name
    $name = l($row->field_profile_name_first . " " . $row->field_profile_name_last, 'node/' . $row->nid);
    $barn = trim($row->field_profile_barn_value);


    // Fetch the profile discipline
    $p_values[0] = $row->nid;
    $profile_result = db_query($query, $p_values);
    $p_row = db_fetch_object($profile_result);
    $discipline = $p_row->field_profile_discipline_value;

    // Fetch the address
    $a_values[0] = $row->field_profile_location_lid;
    $address_result = db_query("SELECT * FROM location WHERE lid = %d", $a_values);
    $a_row = db_fetch_object($address_result);
    $address = '';
    if (trim($a_row->city) <> '') {
      $address = $a_row->city . ", " . $a_row->province;
      if (strtoupper($a_row->country) <> "US") $address .= ", " . strtoupper($a_row->country);
    }

    $content .= '<div class="vendor-node-pro">' . $cr;
    $content .= '<div class="vendor-node-pro-pic">' . $pic . '</div>' . $cr;
    $content .= '<div class="vendor-node-pro-name ">' . $name . '</div>' . $cr;

    if ($address) {
      $content .= '<div class="vendor-node-pro-address">' . $address . '</div>' . $cr;
    }
    if ($barn) {
      $content .= '<div class="vendor-node-pro-barn">Barn: ' . $barn . '</div>' . $cr;
    }

    $content .= '<div class="vendor-node-pro-discipline"> Discipline: ' . $discipline . '</div>' . $cr;
    $content .= '</div>' . $cr;

  }

  $block->content = $content;

  return $block;
}
