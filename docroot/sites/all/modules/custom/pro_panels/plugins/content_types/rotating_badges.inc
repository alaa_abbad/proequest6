<?php
// $Id: Exp $

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Rotating Badges'),
  'icon' => 'icon_node.png',
  'description' => t('Rotating badges for home page.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_rotating_badges_content_type_render($subtype, $conf, $panel_args, $context) {

  $block = new stdClass();
  $block->title = '';
  $node = nodequeue_load_random_node(1);
  $file = array_shift($node->files);
  $image_path = $file->filepath;
  $block->content .= '<img src="/' . $image_path . '">';

  return $block;
}
