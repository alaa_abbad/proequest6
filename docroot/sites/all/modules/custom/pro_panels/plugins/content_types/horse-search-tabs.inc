<?php
/**
 * Plugins definition
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Horse Search Tabs'),
  'icon' => 'icon_node.png',
  'description' => t('Tabs for horse search.'),
  'render callback' => 'pro_panels_horse_search_tabs_content_type_render',
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_horse_search_tabs_content_type_render($subtype, $conf, $panel_args, $context) {
  $block = new stdClass();
  $block->title = '';

  $tabs = array(
    'search/horses' => t('All'),
    'search/horses/hunters' => t('Hunters'),
    'search/horses/jumpers' => t('Jumpers'),
    'search/horses/equitation' => t('Equitation'),
    'search/horses/ponies' => t('Ponies'),
  );

  $block->content = '<ul class="quicktabs-style-proequest">';
  foreach ($tabs as $path => $title) {
    $block->content .= '<li>';
    $block->content .= l($title, $path);
    $block->content .= '</li>';
  }
  $block->content .= '</ul>';

  return $block;
}

