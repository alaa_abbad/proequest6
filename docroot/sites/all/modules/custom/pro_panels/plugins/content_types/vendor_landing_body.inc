<?php
// $Id: Exp $

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'single' => TRUE,
  'title' => t('Vendor Landing Page Body'),
  'icon' => 'icon_node.png',
  'description' => t('Show categories of A-List.'),
  'category' => t('ProEquest'),
);

/**
 * Render the custom content type.
 */
function pro_panels_vendor_landing_body_content_type_render($subtype, $conf, $panel_args, $context) {
  $host = strtolower(reset(explode('/', $_SERVER['SERVER_PROTOCOL']))).'://'.$_SERVER['HTTP_HOST'];

  $cr = chr(10) . chr(13);
  $nid = $panel_args[0];

  $node = node_load($nid);

  // Set link options for vendor logos
  $options['attributes'] = array('target' => '_blank');
  $options['html'] = TRUE;

  $vendor_fb = proequest_fb_like_button();
  $vendor_tweet = proequest_tweet_button();

  $vendor_homepage = l($node->field_vendor_homepage[0]['url'], $node->field_vendor_homepage[0]['url'], $options);

  // Prepare unsold listings
  $viewName = "vendor_products";
  $display_id = "panel_pane_1";
  $args = $node->nid;
  $product_view = views_embed_view($viewName, $display_id, $args);


  $vendor_desc = check_markup($node->body);

  $term_list = taxonomy_get_tree(4);

  $block = new stdClass();
  $block->title = '' . $cr;

  $block->content .= '<div class="vendor-node">' . $cr;
  $block->content .= '<div class="vendor-node-homepage">' . $vendor_homepage . "</div>" . $cr;

  // Show vendors/sponsors
  $block->content .= <<<EOD

    <div class="vendor-logo">
      <div class="vendor-logo-text-background">
        <div class="vendor-logo-text">Browse a carefully selected group of vendors from the trusted providers of the products and services that matter the most to you and your horse.</div>
      </div>
    </div>

    <h1 class="vendor-homepage-title">Browse the A-List</h1>

    <div class="vendor-table">
EOD;
    foreach ($term_list as $term) {
      $term_values = term_fields_get_fields_values($term);
      $term_name = str_replace(' ', '-', $term_values['name_value']);

      $block->content .= '   <div class="vendor-cell"><a href="/vendors/' . $term->name . '"><img src="/sites/all/themes/proequest/images/VendorTerm' . $term_name . '.png" border=0></a></div>' . $cr;
    }

    $block->content .= <<<EOD
    </div> <!-- end of vendor-table -->
  </div> <!-- end of vendor-node -->

EOD;

  return $block;
}
