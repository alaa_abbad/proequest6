<?php
// $Id: twocol_stacked.inc,v 1.1.2.3 2010/06/22 15:54:25 merlinofchaos Exp $

// Plugin definition
$plugin = array(
  'title' => t('Pro Two column stacked big left column'),
  'category' => t('Columns: 2'),
  'icon' => 'pro_twocol_stacked_big_left.png',
  'theme' => 'pro_twocol_stacked_big_left',
  'css' => 'pro_twocol_stacked_big_left.css',
  'panels' => array(
    'top' => t('Top'),
    'left' => t('Left side'),
    'right' => t('Right side'),
    'bottom' => t('Bottom')
  ),
);
