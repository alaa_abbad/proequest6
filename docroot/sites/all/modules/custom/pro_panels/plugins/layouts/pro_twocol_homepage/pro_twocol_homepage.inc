<?php
// $Id: twocol.inc,v 1.1.2.3 2010/06/22 15:54:25 merlinofchaos Exp $

// Plugin definition
$plugin = array(
  'title' => t('Pro two column for homepage'),
  'category' => t('Columns: 2'),
  'icon' => 'twocol.png',
  'theme' => 'pro_panels_twocol_homepage',
  'css' => 'pro_twocol_homepage.css',
  'panels' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
