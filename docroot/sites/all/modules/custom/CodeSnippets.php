      
// For teaser in Story.11/7/11

      <?php if (!is_null($node->field_teaser_image[0]['filepath'])) : ?>
        <div class="news-image-teaser-container">
          <div class="news-image-teaser">
          <?php 
          if ($page == 0) {
            print l($photo_teaser, $url, array('html' => true)); 
          } 
          else {
            print l($photo_full, $url, array('html' => true)); 
          }
          ?>
          </div>
          <?php
          if ($page == 1) print '<div class="news-image-teaser-caption">' . $node->field_teaser_image_caption[0]['value'] . '</div>';
          ?>
        </div> <!-- /news-image-teaser-container -->
      <?php endif ?>
