CONTENTS
- OVERVIEW
- INSTALLATION
- REQUIREMENTS
- CREDITS


OVERVIEW
This module allows you to import imagecache presets in a easy way.

INSTALLATION
- Install like other Drupal Modules.

REQUIREMENTS
- ImageCache Module

USAGE
- Export a present and just paste it on "/admin/build/imagecache/import"

CREDITS:
- Ruben Marques (http://drupal.org/user/757938)
