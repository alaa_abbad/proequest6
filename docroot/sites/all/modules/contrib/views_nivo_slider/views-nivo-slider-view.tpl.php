<?php
// $Id: views-nivo-slider-view.tpl.php,v 1.1.2.1 2010/04/12 19:31:10 pedrofaria Exp $
/**
 * @file
 *  Views Nivo Slider theme wrapper.
 *
 * @ingroup views_templates
 */
?>
<div id="<?php print $views_nivo_slider_id ?>" class="views-nivo-slider clear-block">

  <?php
  $i = 0;
  foreach ($rows as $row): ?>
    <?php
    // turn off the subsequent slides
    $newrow = $row;
    if ($i > 0) {
      $newrow = substr($row, 0, strlen($row) - 7);
      $newrow .= ' style="display: none"></a>';
    }
    print $newrow;
    $i++;

    ?>
  <?php endforeach; ?>
</div>

