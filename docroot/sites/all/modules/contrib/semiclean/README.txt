INSTALL
-------

No specical setup is required. Activate the module like any other.

USAGE
-----

The status is on the Administer > Reports > Status report page.
If the cron semaphore is stuck, the Cron semaphore line should display
a yellow warning status line. Optionally, clean the semaphore right from
the update status page by clicking the "Clean now!" link.

$Id: README.txt,v 1.2 2010/12/13 17:27:03 deekayen Exp $