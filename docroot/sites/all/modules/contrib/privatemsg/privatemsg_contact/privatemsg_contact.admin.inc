<?php
// $Id: privatemsg_contact/privatemsg_contact.admin.inc	24 Jul 2010 15:55:13 -0000 $

/**
 * @file
 * Admin page callbacks for the privatemsg_contact module.
 */

/**
 * Categories/list tab.
 */
function privatemsg_contact_admin_categories() {
  $result = db_query('SELECT * FROM {pm_contact} ORDER BY weight, category');
  $rows = array();
  while ($category = db_fetch_object($result)) {
    $rows[] = array(check_plain($category->category), check_plain($category->recipients), ($category->selected ? t('Yes') : t('No')), l(t('edit'), 'admin/settings/messages/contact/edit/'. $category->cid), l(t('delete'), 'admin/settings/messages/contact/delete/'. $category->cid));
  }
  $header = array(t('Category'), t('Recipients'), t('Selected'), array('data' => t('Operations'), 'colspan' => 2));

  return theme('table', $header, $rows);
}

/**
 * Category edit page.
 */
function privatemsg_contact_admin_edit(&$form_state, $op, $contact = NULL) {

  if (empty($contact) || $op == 'add') {
    $contact = array(
      'category' => '',
      'recipients' => '',
      'weight' => 0,
      'selected' => 0,
      'cid' => NULL,
      'subject_anon' => '!subject',
      'subject_auth' => '!subject',
      'body_anon' => t("(Sent via the site-wide contact form in category !category, by anonymous user !username. Reply to: !mail)\n\n!body"),
      'body_auth' => t("(Sent via the site-wide contact form in category !category)\n\n!body"),
    );
  }
  $form['contact_op'] = array(
    '#type' => 'value',
    '#value' => $op,
  );
  $form['category'] = array(
    '#type' => 'textfield',
    '#title' => t('Category'),
    '#maxlength' => 255,
    '#default_value' => $contact['category'],
    '#description' => t("Example: 'website feedback' or 'product information'."),
    '#required' => TRUE,
  );
  $form['recipients'] = array(
    '#type' => 'textfield',
    '#title' => t('Recipients'),
    '#default_value' => $contact['recipients'],
    '#description' => t("Enter valid user names. Example: 'my_user_name' or 'admin, helpdesk'. To specify multiple recipients, separate each name with a comma."),
    '#required' => TRUE,
    '#size'               => 50,
    '#autocomplete_path'  => 'messages/user-name-autocomplete',
  );
  
  $placeholders = t('You can use the following placeholders: !subject, !body, !username, !mail, !category');

  $form['subject_anon'] = array(
    '#type'  => 'textfield',
    '#title' => t('Subject for anonymous users'),
    '#default_value' => $contact['subject_anon'],
    '#description' => t('Customize the subject of messages sent by anonymous users.') . ' ' . $placeholders,
    '#required'    => TRUE,
    '#size'        => 50,
  );
  $form['body_anon'] = array(
    '#type'  => 'textarea',
    '#title' => t('Body for anonymous users'),
    '#default_value' => $contact['body_anon'],
    '#description' => t('Customize the body of messages sent by anonymous users.') . ' ' . $placeholders,
    '#required'    => TRUE,
    '#size'        => 50,
  );

  $form['subject_auth'] = array(
    '#type'  => 'textfield',
    '#title' => t('Subject for anonymous users'),
    '#default_value' => $contact['subject_auth'],
    '#description' => t('Customize the subject of messages sent by authenticated users.') . ' ' . $placeholders,
    '#required'    => TRUE,
    '#size'        => 50,
  );
  $form['body_auth'] = array(
    '#type'  => 'textarea',
    '#title' => t('Body for anonymous users'),
    '#default_value' => $contact['body_auth'],
    '#description' => t('Customize the body of messages sent by authenticated users.') . ' ' . $placeholders,
    '#required'    => TRUE,
    '#size'        => 50,
  );

  $form['weight'] = array(
    '#type' => 'weight',
    '#title' => t('Weight'),
    '#default_value' => $contact['weight'],
    '#description' => t('When listing categories, those with lighter (smaller) weights get listed before categories with heavier (larger) weights. Categories with equal weights are sorted alphabetically.'),
  );
  $form['selected'] = array(
    '#type' => 'select',
    '#title' => t('Selected'),
    '#options' => array('0' => t('No'), '1' => t('Yes')),
    '#default_value' => $contact['selected'],
    '#description' => t('Set this to <em>Yes</em> if you would like this category to be selected by default.'),
  );
  $form['cid'] = array(
    '#type' => 'value',
    '#value' => $contact['cid'],
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Validate the contact category edit page page.
 */
function privatemsg_contact_admin_edit_validate($form, &$form_state) {
  $exists = db_result(db_query("SELECT 1 FROM {pm_contact} WHERE category = '%s'", $form_state['values']['category']));
  if ($exists) {
    form_set_error('category', t('A contact form with category %category already exists.', array('%category' => $form_state['values']['category'])));
  }
}

/**
 * Process the contact category edit page form submission.
 */
function privatemsg_contact_admin_edit_submit($form, &$form_state) {
  if ($form_state['values']['selected']) {
    // Unselect all other contact categories.
    db_query('UPDATE {pm_contact} SET selected = 0');
  }
  // Notify about invalid recipients, but store them anyway.
  list($recipients, $invalid) = _privatemsg_parse_userstring($form_state['values']['recipients']);
  if (!empty($invalid)) {
    drupal_set_message(t('The following recipients are invalid: @invalid', array('@invalid' => implode(", ", $invalid))), 'warning');
  }
  if (empty($form_state['values']['cid']) || $form_state['values']['contact_op'] == 'add') {
    drupal_write_record('pm_contact', $form_state['values']);
    drupal_set_message(t('Category %category has been added.', array('%category' => $form_state['values']['category'])));
    watchdog('mail', 'Privatemsg Contact form: category %category added.', array('%category' => $form_state['values']['category']), WATCHDOG_NOTICE, l(t('view'), 'admin/settings/messages/contact'));

  }
  else {
    drupal_write_record('pm_contact', $form_state['values'], 'cid');
    drupal_set_message(t('Category %category has been updated.', array('%category' => $form_state['values']['category'])));
    watchdog('mail', 'Privatemsg Contact form: category %category updated.', array('%category' => $form_state['values']['category']), WATCHDOG_NOTICE, l(t('view'), 'admin/settings/messages/contact'));
  }

  $form_state['redirect'] = 'admin/settings/messages/contact';
  return;
}

/**
 * Helper function to translate comma separated list of usernames into
 * array of uid's. If invalid username is found, an error is filed
 * against the supplied form element (if any).
 */
function privatemsg_contact_pack_users($names, $form_element = NULL) {
  $names = explode(',', $names);
  $uids = array();
  foreach ($names as $name) {
    $name = trim($name);
    $recipient = db_fetch_object(db_query("SELECT uid, name FROM {users} WHERE name = '%s'", $name));
    if (!empty($name) && !empty($recipient)) {
      $uids[] = $recipient->uid;
    }
    else if (!empty($name) && !empty($form_element)) {
      form_set_error($form_element, t('%recipient is not an existing user on this site.', array('%recipient' => trim($name))));
    }
  }
  return $uids;
}

/**
 * Category delete page.
 */
function privatemsg_contact_admin_delete(&$form_state, $contact) {

  $form['contact'] = array(
    '#type' => 'value',
    '#value' => $contact,
  );

  return confirm_form($form, t('Are you sure you want to delete %category?', array('%category' => $contact['category'])), 'admin/settings/messages/contact', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Process category delete form submission.
 */
function privatemsg_contact_admin_delete_submit($form, &$form_state) {
  $contact = $form_state['values']['contact'];
  db_query("DELETE FROM {pm_contact} WHERE cid = %d", $contact['cid']);
  drupal_set_message(t('Category %category has been deleted.', array('%category' => $contact['category'])));
  watchdog('mail', 'Privatemsg Contact form: category %category deleted.', array('%category' => $contact['category']), WATCHDOG_NOTICE);

  $form_state['redirect'] = 'admin/settings/messages/contact';
  return;
}

/**
 * Global settings page.
 */
function privatemsg_contact_admin_settings() {
  $form['privatemsg_contact_form_information'] = array(
    '#type' => 'textarea',
    '#title' => t('Additional information'),
    '#default_value' => variable_get('privatemsg_contact_form_information', t('You can leave a message using the contact form below.')),
    '#description' => t('Information to show on the <a href="@form">contact page</a>. Can be anything from submission guidelines to your postal address or telephone number.', array('@form' => url('privatemsg_contact'))),
  );
  $form['privatemsg_contact_hourly_threshold'] = array(
    '#type' => 'select',
    '#title' => t('Hourly threshold'),
    '#options' => drupal_map_assoc(array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40, 50)),
    '#default_value' => variable_get('privatemsg_contact_hourly_threshold', 3),
    '#description' => t('The maximum number of contact form submissions a user can perform per hour.'),
  );
  $sender = user_load(variable_get('privatemsg_contact_anonymous_sender', 1));
  $form['privatemsg_contact_anonymous_sender'] = array(
    '#type' => 'textfield',
    '#title' => t('Send anonymous messages on behalf of user'),
    '#maxlength' => 255,
    '#default_value' => $sender->name,
    '#description' => t("When anonymous users are given permission to use the site-wide contact form, all anonymous messages will get this user as a sender instead. Ideally, this should be a dummy user account created solely for this purpose, such as 'contact form', 'no reply', 'anonymous feedback', or similar. This setting has no effect on messages submitted by authenticated users."),
    '#required' => TRUE,
  );
  $form['#validate'][] = 'privatemsg_contact_settings_validate';
  $form['#submit'][] = 'privatemsg_contact_settings_submit';
  return system_settings_form($form);
}

/**
 * Form validation handler. Checks the anonymous sender field.
 */
function privatemsg_contact_settings_validate($form, &$form_state) {
  $uids = privatemsg_contact_pack_users($form_state['values']['privatemsg_contact_anonymous_sender'], 'privatemsg_contact_anonymous_sender');
  if (count($uids) != 1) {
    form_set_error('privatemsg_contact_anonymous_sender', t('You must enter exactly one valid user name.'));
  }
}

/**
 * Form submit handler. Converts the anonymous sender field into uid.
 */
function privatemsg_contact_settings_submit($form, &$form_state) {
  $uids = privatemsg_contact_pack_users($form_state['values']['privatemsg_contact_anonymous_sender']);
  $form_state['values']['privatemsg_contact_anonymous_sender'] = reset($uids);
}