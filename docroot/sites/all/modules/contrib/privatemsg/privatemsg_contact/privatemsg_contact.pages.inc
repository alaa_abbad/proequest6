<?php
// $Id: privatemsg_contact/privatemsg_contact.pages.inc	24 Jul 2010 15:55:14 -0000 $

/**
 * @file
 * User page callbacks for the privatemsg_contact module.
 */


/**
 * Site-wide contact page.
 */
function privatemsg_contact_site_page() {
  global $user;

  if (!flood_is_allowed('privatemsg_contact', variable_get('privatemsg_contact_hourly_threshold', 3))) {
    drupal_set_message(t("You cannot send more than %number messages per hour. Try again later.", array('%number' => variable_get('privatemsg_contact_hourly_threshold', 3))));
    drupal_access_denied();
  }
  else {
    return drupal_get_form('privatemsg_contact_mail_page');
  }

  return '';
}

function privatemsg_contact_mail_page() {
  global $user;

  $categories = array();
  $default_category = 0;
  $result = db_query('SELECT cid, category, selected FROM {pm_contact} ORDER BY weight, category');
  while ($category = db_fetch_object($result)) {
    $categories[$category->cid] = check_plain($category->category);
    if ($category->selected) {
      $default_category = $category->cid;
    }
  }


  if (count($categories) > 0) {
    $form['#token'] = $user->uid ? $user->name . $user->mail : '';
    $form['contact_information'] = array('#value' => filter_xss_admin(variable_get('privatemsg_contact_form_information', t('You can leave a message using the contact form below.'))));
    if (!$user->uid) {
      $form['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Your name'),
        '#maxlength' => 255,
        '#required' => TRUE,
      );
      $form['mail'] = array(
        '#type' => 'textfield',
        '#title' => t('Your e-mail address'),
        '#maxlength' => 255,
        '#required' => TRUE,
      );
    }
    $form['subject'] = array(
      '#type' => 'textfield',
      '#title' => t('Subject'),
      '#maxlength' => 255,
      '#required' => TRUE,
    );

    if (count($categories) > 1) {
      // If there is more than one category available and no default category has been selected,
      // prepend a default placeholder value.
      if (empty($default_category)) {
        $categories = array(t('- Please choose -')) + $categories;
      }
      $form['cid'] = array(
        '#type' => 'select',
        '#title' => t('Category'),
        '#default_value' => $default_category,
        '#options' => $categories,
        '#required' => TRUE,
      );
    }
    else {
      // If there is only one category, store its cid.
      $category_key = key($categories);
      $form['cid'] = array(
        '#type' => 'value',
        '#value' => $category_key,
      );
    }
    $form['message'] = array(
      '#type' => 'textarea',
      '#title' => t('Message'),
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Send message'),
    );
    return $form;
  }
  else {
    if (privatemsg_user_access('administer privatemsg settings')) {
      drupal_set_message(t('The contact form has not been configured. <a href="@add">Add one or more categories</a> to the form.', array('@add' => url('admin/settings/messages/contact/add'))), 'error');
    }
    else {
      drupal_access_denied();
    }
  }
  return '';
}

/**
 * Validate the site-wide contact page form submission.
 */
function privatemsg_contact_mail_page_validate($form, &$form_state) {
  global $user;
  if (empty($form_state['values']['cid'])) {
    form_set_error('cid', t('You must select a valid category.'));
  }
  if (!$user->uid && !valid_email_address($form_state['values']['mail'])) {
    form_set_error('mail', t('You must enter a valid e-mail address.'));
  }
}

/**
 * Process the site-wide contact page form submission.
 */
function privatemsg_contact_mail_page_submit($form, &$form_state) {
  global $user;
  $values = $form_state['values'];

  // Load the contact category.
  $contact = privatemsg_contact_load($values['cid']);

  // Prepare placeholder values.
  $placeholders = array(
    '!body' => $values['message'],
    '!subject' => $values['subject'],
    '!username' => empty($user->uid) ? $values['name'] : $user->name,
    '!mail' => empty($user->uid) ? $values['mail'] : $user->mail,
    '!category' => $contact['category'],
  );

  if (!$user->uid) {
    $body = t($contact['body_anon'], $placeholders);
    $subject = t($contact['subject_anon'], $placeholders);
  }
  else {
    $body = t($contact['body_auth'], $placeholders);
    $subject = t($contact['subject_auth'], $placeholders);
  }

  // Override author, if anonymous
  $options = array();
  if (!$user->uid) {
    $options = array('author' => user_load(variable_get('privatemsg_contact_anonymous_sender', 1)));
  }

  // Send the message using privatemsg API.
  privatemsg_new_thread($contact['users'], $subject, $body, $options);

  flood_register_event('privatemsg_contact');
  watchdog('mail', '%name-from sent a message regarding %category.', array('%name-from' => $user->uid ? $user->name : ($values['name'] .' ['. $values['mail'] .']'), '%category' => $contact['category']));
  drupal_set_message(t('Your message has been sent.'));

  // Jump to home page rather than back to contact page to avoid
  // contradictory messages if flood control has been activated.
  $form_state['redirect'] = '';
}
